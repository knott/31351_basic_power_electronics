# Course material for 31351 Basic Power Electronics
This repository contains

- [Slides](slides_and_script/output/presentation.pdf)
- [Script](slides_and_script/output/script.pdf)
<!-- - [Guest Lectures](guest_lectures/)
- [Extra Material](extra_material/) -->

and the [LaTeX sources](slides_and_script/src/) for the slides and the script.

# [LICENSE](LICENSE)
