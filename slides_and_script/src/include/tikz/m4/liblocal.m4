divert(-1)
	liblocal.m4

* local defined macros, copyright by Arnold Knott    *
* This file defines additional circuit macros        *
* I hope that the contents of this repository/file   *
* is of any use for you. So feel free to use it,     *
* copy it and redistribute it. Whatever you do       *
* alwyas leave this notice. There is no warramty     *
* WHATSOEVER.  Not even any implied warranty for     *
* MERCANTABILITY or FITNESS FOR A PARTICULAR PURPOSE *

	`pvcell( linespec, wid, ht )'
define(`pvcell',`eleminit_(`$1')
  define(`m4wd',ifelse(`$2',,`dimen_/2',`($2)'))dnl 
  define(`m4ht',ifelse(`$3',,`dimen_/5',`($3)'))dnl
  { line to rvec_(max(0,rp_len/2-m4wd/2),0)
    {[lbox(m4wd,m4ht)] at rvec_(m4wd/2,0)}
    {line from rvec_(0,-m4ht/2) to rvec_(m4wd/3,0) then to rvec_(0,m4ht/2)}
    move to rvec_(m4wd,0); line to rvec_(max(0,rp_len/2-m4wd/2),0) }
  line invis to rvec_(rp_len,0)')

    `impedance( linespec )'
define(`impedance',`eleminit_(`$1')
  define(`m4ht',`dimen_/5')dnl
  define(`m4wd',`m4ht*2')dnl
  {line to rvec_(max(0,rp_len/2-m4wd/2),0)
  	{[lbox(m4wd,m4ht)] at rvec_(m4wd/2,0)}
	{arc cw from rvec_(m4wd/4,0) to rvec_(m4wd/2,0) rad m4wd/7}
	{arc ccw from rvec_(m4wd/2,0) to rvec_(m4wd*3/4,0) rad m4wd/7}
	move to rvec_(m4wd,0); line to rvec_(max(0,rp_len/2-m4wd/2),0)
  }
  {[box invis ht_ m4ht wid_ m4wd] at rvec_(rp_len/2,0)}
  line to rvec_(rp_len,0) invis
')

right_
divert(0)dnl
