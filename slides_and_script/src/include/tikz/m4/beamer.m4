divert(-1)
  beamer.m4

* local defined macros, copyright by Arnold Knott    *
* This file defines additional circuit macros        *
* I hope that the contents of this repository/file   *
* is of any use for you. So feel free to use it,     *
* copy it and redistribute it. Whatever you do       *
* alwyas leave this notice. There is no warramty     *
* WHATSOEVER.  Not even any implied warranty for     *
* MERCANTABILITY or FITNESS FOR A PARTICULAR PURPOSE *

define(`beamer_')

##### add uncover for animating slides with beamer
## usage:
## setuncover(start,end)
## <whatever code should be animated>
## resetuncover
##
## where `start' is first slide to show and `end' is last slide (if all, use `-')
dnl   `setuncover(start,end)'
define(`setuncover', `command sprintf("\uncover<$1$2>{")')
dnl   `resetuncover()'
define(`resetuncover', `command sprintf("}")')

right_
divert(0)dnl
