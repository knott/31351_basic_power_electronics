\section<presentation>[Noniso Conv]{Nonisolated Converters}
\section<article>{Nonisolated Converters}
\label{sec:nonisoconv}

% \begin{frame}
%   \sectionpage
%   \tableofcontents[hideothersubsections]
% \end{frame}

\subsection[Buck]{Buck Converter}
\label{sub:buck}

\begin{frame}
  \frametitle{Buck}
  \begin{block}{Circuit diagram}
    \centering
    \input{tikz/buck.tikz.tex}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Buck - operation modes}
  \begin{block}{$0 < t < dT$}
    \centering
    \input{tikz/buck_on.tikz.tex}
  \end{block}
  \begin{block}{Equivalent circuit diagram during $0 < t < dT$}
    \centering
    \input{tikz/buck_equiv_on.tikz.tex}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Buck - operation modes}
  \begin{block}{$dT < t < T$}
    \centering
    \input{tikz/buck_off.tikz.tex}
  \end{block}
  \begin{block}{Equivalent circuit diagram during $dT < t < T$}
    \centering
    \input{tikz/buck_equiv_off.tikz.tex}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Buck - averaging}
  \begin{block}{Assumptions}
    \begin{itemize}
      \item steady-state operation
      \item signals are ripple free
      \item repetitive operation
      \item linearization around bias point
      \item all signals are either DC or rectangular
      \item all time-derivatives are zero
    \end{itemize}
    $\Rightarrow$ analyzing the average values
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Buck - averaging}
  \begin{columns}[T]
    \column{.48\framewidth}
    \begin{derivation}{$0 < t < dT$}
      inductor voltage:
      \uncover<2->
      {
        \begin{align*}
          V_{in} - V_C
        \end{align*}
      }
      capacitor current:
      \uncover<4->
      {
        \begin{align*}
          I_L - I_{out}
        \end{align*}
      }
      \vspace{-0.3cm}
    \end{derivation}
    \only<-5>
    {
      \begin{block}{Equivalent circuit diagram during $0 < t < dT$}
        \centering
        \everymath{\scriptstyle}
        \small
        \input{tikz/buck_equiv_on_scale.tikz.tex}
      \end{block}
    }
    \column{.48\framewidth}
    \begin{derivation}{$dT < t < T$}
      inductor voltage:
      \uncover<3->
      {
        \begin{align*}
          -V_C
        \end{align*}
      }
      capacitor current:
      \uncover<5->
      {
        \begin{align*}
          I_L - I_{out} 
        \end{align*}
      }
      \vspace{-0.3cm}
    \end{derivation}
    \only<-5>
    {
      \begin{block}{Equivalent circuit diagram during $dT < t < T$}
        \centering
        \everymath{\scriptstyle}
        \small
        \input{tikz/buck_equiv_off_scale.tikz.tex}
      \end{block}
    }
  \end{columns}
  \uncover<6->
  {
    \begin{derivation}{average inductor voltage \& capacitor current}
      \begin{align*}
        V_{L_{avg}} = d T ( V_{in} - V_C) - ( 1 - d ) T V_C \stackrel{!}{=} 0\\
        I_{C_{avg}} = d T ( I_{L} - I_{out}) + ( 1 - d ) T ( I_L - I_{out}) \stackrel{!}{=} 0
      \end{align*}
    \end{derivation}
  }
  \uncover<7->
  {
    \begin{important}{DC-transfer function of buck}
      \begin{align}
        \frac{V_C}{V_{in}} = d & \hspace{3em} & I_{out} = I_L
      \end{align}
    \end{important}
  }
\end{frame}

\begin{frame}
  \frametitle{Buck}
  \begin{block}{Black box approach}
    assumption: efficiency $\eta = 100~\%$
    \begin{align*}
      \frac{I_{in}}{I_{out}} = \frac{ \frac{P}{V_{in}} }{ \frac{P}{V_{out}} } = \frac{V_{out}}{V_{in}} = d
    \end{align*}
  \end{block}
  \begin{important}{Input current}
    \begin{align}
      I_{in} = d I_{out} = d I_L
    \end{align}
  \end{important}
\end{frame}

\begin{frame}[shrink = 0.8]
  \frametitle{Superposition of ripple $=$ adding AC}
  \begin{derivation}{$0 < t < dT$}
    \uncover<1->
    {
      inductor current ripple:
      \begin{align*}
        i_L( t ) = &\frac{ \int_0^{dT} \left( V_{in} -V_C \right) \delta t}{ L } + i_L( 0 ) = dT \frac{ V_{in} -V_C }{ L } + i_L( 0 )
      \end{align*}
    }
    \uncover<2->
    {
      \hspace{-1em} capacitor voltage ripple:
      \begin{align*}
        v_C( t ) &= \frac{ \int_0^{dT} \left( i_{L} ( t ) - I_{out} \right) \delta t}{ C } + v_C( 0 ) \\
                 & =  \frac{ \int_0^{dT} \left( \overline{{i}}_{L} ( t ) + \tilde{{i}}_{L} ( t ) - I_{out} \right) \delta t}{ C } + v_C( 0 )\\
                 % & \stackrel{I_L=I_{out}}{=} \underbrace{ \frac{ \int_0^t + \tilde{{i}}_{L} ( t ) \delta t}{ C } }_{\tilde{v}_C(t)} + v_C ( 0 )
                 & = \underbrace{ \frac{ \int_0^{dT} \tilde{{i}}_{L} ( t ) \delta t}{ C } }_{\tilde{v}_C(t)} + v_C ( 0 )\\
        \tilde{v}_C(t) & = \frac{ \int_0^{dT} \int_0^{dT} \left( V_{in} -V_C \right) \delta t \delta t}{ L C } + \frac{ \int_0^{dT} i_L( 0 ) \delta t}{ C }\\
                 & = \frac{ V_{in} - V_C }{ 2 L C } t^2 + \frac{ i_L( 0 ) }{ C } t
      \end{align*}
    }
  \end{derivation}
\end{frame}

\begin{frame}
  \frametitle{Superposition of ripple $=$ adding AC}
  \begin{derivation}{$dT < t < T$}
    \uncover<1->
    {
      inductor current ripple:
      \begin{align*}
        i_L( t ) = &\frac{ - \int_{dT}^T V_C \delta t}{ L } + i_L( dT ) = -( 1 - d ) T \frac{ V_C }{ L } + i_L( dT )
      \end{align*}
    }
    \uncover<2->
    {
      \hspace{-1em} capacitor voltage ripple:
      \begin{align*}
        & \ldots\\
        \tilde{v}_C(t) = & - \frac{ V_C }{ 2 L C } t^2 +  \frac{ i_L( dT ) }{ C } t
      \end{align*}
    }
  \end{derivation}
\end{frame}

\begin{frame}
  \frametitle{Buck}
  \begin{block}{Signals}
    \centering
    \input{tikz/buck_signals.tikz.tex}\newline
    \uncover<2->
    {
      When $I_L > 0 \Rightarrow$ Continuous Conduction Mode (CCM)
    }
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Buck - all operating modes}
  \begin{columns}[T]
    \column{.31\framewidth}
    \begin{block}{Continuous Conduction Mode (CCM)}
      \input{tikz/cont_cond_mode.tikz.tex}
    \end{block}
    \column{.31\framewidth}
    \begin{block}{Boundary Conduction Mode (BCM)}
      \input{tikz/bound_cond_mode.tikz.tex}
      also called Critical Conduction Mode (CrCM) (sometimes abbreviated CCM)
    \end{block}
    \column{.31\framewidth}
    \begin{block}{Discontinuous Conduction Mode (DCM)}
      \input{tikz/discont_cond_mode.tikz.tex}
      analysis above not valid here! (later in course!)
    \end{block}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Buck - Exercise}
  \begin{exercise}{Ideal Buck...}
    An ideal buck converter is operating from $V_{in} = ~30~\si{\volt}$ and delivers $I_{out} = 10~\si{\ampere}$. The inductor current is constant.
    \begin{enumerate}[a]
    \item What is the output voltage if the duty cycle $d = 0.175$?
    \item Sketch the current in the active power semiconductors.
    \end{enumerate}
  \end{exercise}
\end{frame}

\begin{frame}
  \frametitle{Buck - Exercise}
  \begin{columns}[T]
    \column{0.48\framewidth}
    \addtocounter{exercise}{-1}
    \begin{exercise}{$+$ diode losses}
      \begin{enumerate}[a]
        \setcounter{enumi}{2}
        \item How big are the conduction losses in the diode for this VI-characteristic:
      \end{enumerate}
      \input{tikz/ex_buck_diode_VI.tikz.tex}
    \end{exercise}
    \column{0.48\framewidth}
    \addtocounter{exercise}{-1}
    \begin{exercise}{$+$ FET losses}
      \begin{enumerate}[a]
        \setcounter{enumi}{3}
        \item  How big are the conduction losses in the MOSFET for  junction temperature $\vartheta_j = 100~\si{\degreeCelsius}$ for this on-characteristic:\vspace{-1em}
      \end{enumerate}
      \input{tikz/ex_buck_RDSon_theta.tikz.tex}
    \end{exercise}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Buck - Exercise}
  From now on assume, that the total losses in the MOSFET are $P_{d_{MOSFET}} = 9.3~\si{\watt}$.
  \addtocounter{exercise}{-1}
  \begin{exercise}{dimension heatsink for MOSFET}
    \begin{enumerate}[a]
      \setcounter{enumi}{4}
      \item The junction temperature of the MOSFET shall be kept at $\vartheta_j = 100~\si{\degreeCelsius}$. The inner thermal resistance of the package is $R_{th_{jc}} = 3~\frac{\si{\kelvin}}{\si{\watt}}$ and the maximum ambient temperature is $\vartheta_a = 40~\si{\degreeCelsius}$. What is the maximum thermal resistance $R_{th_{ca}}$ of the heatsink?
      \item What is the maximum thermal resistance of another heatsink, if both the MOSFET and the diode are mounted to the same heatsink (same conditions as above, diode switching losses are ignored)?
    \end{enumerate}
  \end{exercise}
\end{frame}

\begin{frame}[shrink=0.8]
  \frametitle{Buck - Exercise}
  \begin{exercise}{Another ideal buck}
    An ideal buck converter is switching with $f_{sw} = 100~\si{\kilo\hertz}$. The input voltage $V_{in} = 5~\si{\volt}$ and the load is modelled with a resistance $R_L = 0.1~\si{\ohm}$. Inductor current and capacitor voltage do not have ripple.
    \begin{enumerate}[a]
      \item What is the maximum theoretical efficiency $\eta$? Why?
      \item What is the required duty cycle $d$ to get an output voltage $V_{out} = 1.5~\si{\volt}$?
      \item After the input voltage rose, $d$ gets adjusted to $25~\%$ to keep the same output voltage. What is the peak input current $\hat{I}_{in}$?
      \item What is the average value of the input current $\overline{I}_{in}$?
      \item What is the RMS current of the input current $I_{in_{RMS}}$?
      \item Draw the diode current.
      \item Everything else unchanged, the MOSFET is now modelled with an on-resistance of $R_{DS_{on}} = 10~\si{\milli\ohm}$. How much power is dissipated in the MOSFET?
    \end{enumerate}
  \end{exercise}
\end{frame}

\subsection[Boost]{Boost Converter}
\label{sub:boost}

\begin{frame}
  \frametitle{Boost}
  \begin{block}{Circuit diagram}
    \centering
    \input{tikz/boost.tikz.tex}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Boost - operation modes}
  \begin{block}{$0 < t < dT$}
    \centering
    \input{tikz/boost_on.tikz.tex}
  \end{block}
  \begin{block}{Equivalent circuit diagram during $0 < t < dT$}
    \centering
    \input{tikz/boost_equiv_on.tikz.tex}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Boost - operation modes}
  \begin{block}{$dT < t < T$}
    \centering
    \input{tikz/boost_off.tikz.tex}
  \end{block}
  \begin{block}{Equivalent circuit diagram during $dT < t < T$}
    \centering
    \input{tikz/boost_equiv_off.tikz.tex}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Boost - averaging}
  \begin{columns}[T]
    \column{.48\framewidth}
    \begin{derivation}{$0 < t < dT$}
      inductor voltage:
      \uncover<2->
      {
        \begin{align*}
          V_{in}
        \end{align*}
      }
      capacitor current:
      \uncover<4->
      {
        \begin{align*}
          - I_{out}
        \end{align*}
      }
      \vspace{-0.3cm}
    \end{derivation}
    \only<-5>
    {
      \begin{block}{Equivalent circuit diagram during $0 < t < dT$}
        \centering
        \everymath{\scriptstyle}
        \small
        \input{tikz/boost_equiv_on_scale.tikz.tex}
      \end{block}
    }
    \column{.48\framewidth}
    \begin{derivation}{$dT < t < T$}
      inductor voltage:
      \uncover<3->
      {
        \begin{align*}
          V_{in} -V_C
        \end{align*}
      }
      capacitor current:
      \uncover<5->
      {
        \begin{align*}
          I_L - I_{out} 
        \end{align*}
      }
      \vspace{-0.3cm}
    \end{derivation}
    \only<-5>
    {
      \begin{block}{Equivalent circuit diagram during $dT < t < T$}
        \centering
        \everymath{\scriptstyle}
        \small
        \input{tikz/boost_equiv_off_scale.tikz.tex}
      \end{block}
    }
  \end{columns}
  \uncover<6->
  {
    \begin{derivation}{average inductor voltage \& capacitor current}
      \begin{align*}
        V_{L_{avg}} = & d T V_{in} + ( 1 - d ) T ( V_{in} - V_C) \stackrel{!}{=} 0\\
                      & V_{in} = ( 1 - d ) V_C\\
        I_{C_{avg}} = & - d T I_{out} + ( 1 - d ) T ( I_L - I_{out}) \stackrel{!}{=} 0\\
                      & -I_{out} = - (1 - d) I_L
      \end{align*}
    \end{derivation}
  }
\end{frame}

\begin{frame}
  \frametitle{Boost}
  \begin{important}{DC-transfer function of boost}
    \begin{align}
      \frac{V_C}{V_{in}} = \frac{1}{1 - d}\\
      \vspace{2em}
      \frac{I_L}{I_{out}} = \frac{1}{1 - d}
    \end{align}
  \end{important}
  \begin{columns}[T]
    \column{.31\framewidth}
    \uncover<1->
    {
      \begin{block}{$1/d$}
        \centering
        \input{tikz/boost_xfer1.tikz.tex}
      \end{block}
    }
    \column{.31\framewidth}
    \uncover<2->
    {
      \begin{block}{$1/-d$}
        \centering
        \input{tikz/boost_xfer2.tikz.tex}
      \end{block}
    }
    \column{.31\framewidth}
    \uncover<3->
    {
      \begin{block}{$1/(1-d)$}
        \centering
        \input{tikz/boost_xfer3.tikz.tex}
      \end{block}
    }
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Boost}
  \begin{block}{Black box approach}
    assumption: efficiency $\eta = 100~\%$
    \begin{align*}
      I_{in} = \frac{ V_{out}}{ V_{in} } I_{out} = \frac{ 1 }{ 1 - d } I_{out}
    \end{align*}
    with
    \begin{align*}
      I_{L} = \frac{ 1 }{ 1 - d } I_{out} \Rightarrow I_{out} = ( 1 - d ) I_L
    \end{align*}
  \end{block}
  \begin{important}{Input current}
    \begin{align}
      I_{in} = I_L
    \end{align}
  \end{important}
\end{frame}

\begin{frame}[shrink = 0.8]
  \frametitle{Superposition of ripple $=$ adding AC}
  \begin{derivation}{$0 < t < dT$}
    \uncover<1->
    {
      inductor current ripple:
      \begin{align*}
        i_L( t ) = &\frac{ \int_0^{dT} \left( V_{in} \right) \delta t}{ L } + i_L( 0 ) = dT \frac{ V_{in} }{ L } + i_L( 0 )
      \end{align*}
    }
    \uncover<2->
    {
      \hspace{-1em} capacitor voltage ripple:
      \begin{align*}
        v_C( t ) &= \frac{ - \int_0^{dT} I_{out} \delta t}{ C } + v_C( 0 )  = -dT \frac{ I_{out} }{ C } + v_C( 0 )\\
      \end{align*}
    }
  \end{derivation}
\end{frame}

\begin{frame}[shrink = 0.5]
  \frametitle{Superposition of ripple $=$ adding AC}
  \begin{derivation}{$dT < t < T$}
    \uncover<1->
    {
      inductor current ripple:
      \begin{align*}
        i_L( t ) = &\frac{ \int_{dT}^{T} \left( V_{in} -V_C \right) \delta t}{ L } + i_L( dT ) = dT \frac{ V_{in} -V_C }{ L } + i_L( dT )
      \end{align*}
    }
    \uncover<2->
    {
      \hspace{-1em} capacitor voltage ripple:
      \begin{align*}
        v_C( t ) &= \frac{ \int_{dT}^{T} \left( i_{L} ( t ) - I_{out} \right) \delta t}{ C } + v_C( dT ) \\
                 % & =  \frac{ \int_{dT}^{T} \left( \overline{{i}}_{L} ( t ) + \tilde{{i}}_{L} ( t ) - I_{out} \right) \delta t}{ C } + v_C( dT )\\
                 % & \stackrel{I_L=I_{out}}{=} \underbrace{ \frac{ \int_0^t + \tilde{{i}}_{L} ( t ) \delta t}{ C } }_{\tilde{v}_C(t)} + v_C ( 0 )
                 & = \underbrace{ \frac{ \int_{dT}^{T} \left( i_{L} ( t ) - I_{out} \right) \delta t}{ C } }_{\tilde{v}_C(t)} + v_C ( dT )\\
        \tilde{v}_C(t) & = \frac{ \int_{dT}^{T} \int_{dT}^{T} \left( V_{in} -V_C \right) \delta t \delta t}{ L C } + \frac{ \int_{dT}^{T} \left( i_L( dT ) - I_{out} \right) \delta t}{ C }\\
                 & = \frac{ V_{in} - V_C }{ 2 L C } t^2 +   i_L( dT ) - \frac{ I_{out} }{ C } t
      \end{align*}
    }
  \end{derivation}
\end{frame}

\begin{frame}
  \frametitle{Boost}
  \begin{block}{Signals}
    \centering
    \input{tikz/boost_signals.tikz.tex}\newline
    \uncover<2->
    {
      When $I_L > 0 \Rightarrow$ Continuous Conduction Mode (CCM)
    }
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Boost - all operating modes}
  \begin{columns}[T]
    \column{.31\framewidth}
    \begin{block}{Continuous Conduction Mode (CCM)}
      \input{tikz/cont_cond_mode.tikz.tex}
    \end{block}
    \column{.31\framewidth}
    \begin{block}{Boundary Conduction Mode (BCM)}
      \input{tikz/bound_cond_mode.tikz.tex}
      also called Critical Conduction Mode (CrCM) (sometimes abbreviated CCM)
    \end{block}
    \column{.31\framewidth}
    \begin{block}{Discontinuous Conduction Mode (DCM)}
      \input{tikz/discont_cond_mode.tikz.tex}
      analysis above not valid here! (later in course!)
    \end{block}
  \end{columns}
\end{frame}

\subsection[Buck-boost]{Buck-boost Converter}
\label{sub:buckboost}

\begin{frame}
  \frametitle{Buck-boost}
  \begin{block}{Circuit diagram}
    \centering
    \input{tikz/buckboost.tikz.tex}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Buck-boost - operation modes}
  \begin{block}{$0 < t < dT$}
    \centering
    \input{tikz/buckboost_on.tikz.tex}
  \end{block}
  \begin{block}{Equivalent circuit diagram during $0 < t < dT$}
    \centering
    \input{tikz/buckboost_equiv_on.tikz.tex}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Buck-boost - operation modes}
  \begin{block}{$dT < t < T$}
    \centering
    \input{tikz/buckboost_off.tikz.tex}
  \end{block}
  \begin{block}{Equivalent circuit diagram during $dT < t < T$}
    \centering
    \input{tikz/buckboost_equiv_off.tikz.tex}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Buck-boost - averaging}
  \begin{columns}[T]
    \column{.48\framewidth}
    \begin{derivation}{$0 < t < dT$}
      inductor voltage:
      \uncover<2->
      {
        \begin{align*}
          V_{in}
        \end{align*}
      }
      capacitor current:
      \uncover<4->
      {
        \begin{align*}
          - I_{out}
        \end{align*}
      }
      \vspace{-0.3cm}
    \end{derivation}
    \only<-5>
    {
      \begin{block}{Equivalent circuit diagram during $0 < t < dT$}
        \centering
        \everymath{\scriptstyle}
        \small
        \input{tikz/buckboost_equiv_on_scale.tikz.tex}
      \end{block}
    }
    \column{.48\framewidth}
    \begin{derivation}{$dT < t < T$}
      inductor voltage:
      \uncover<3->
      {
        \begin{align*}
          -V_C
        \end{align*}
      }
      capacitor current:
      \uncover<5->
      {
        \begin{align*}
          I_L - I_{out} 
        \end{align*}
      }
      \vspace{-0.3cm}
    \end{derivation}
    \only<-5>
    {
      \begin{block}{Equivalent circuit diagram during $dT < t < T$}
        \centering
        \everymath{\scriptstyle}
        \small
        \input{tikz/buckboost_equiv_off_scale.tikz.tex}

      \end{block}
    }
  \end{columns}
  \uncover<6->
  {
    \begin{derivation}{average inductor voltage \& capacitor current}
      \begin{align*}
        V_{L_{avg}} = d T V_{in} - ( 1 - d ) T V_C \stackrel{!}{=} 0\\
        I_{C_{avg}} = - d T I_{out} + ( 1 - d ) T ( I_L - I_{out}) \stackrel{!}{=} 0
      \end{align*}
    \end{derivation}
  }
\end{frame}

\begin{frame}
  \frametitle{Buck-boost - averaging}
  \begin{important}{DC-transfer function of buckboost}
    \begin{align}
      \frac{V_C}{V_{in}} = \frac{ d }{ 1 - d } & \hspace{3em} & \frac{I_L}{I_{out}} = \frac{ 1 }{ 1 - d } 
    \end{align}
  \end{important}
  \begin{block}{$ d / ( 1 - d )$}
    \centering
    \input{tikz/buckboost_xfer.tikz.tex}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Buck-boost}
  \begin{block}{Black box approach under assumption: efficiency $\eta = 100~\%$}
    \begin{align*}
      I_{in} = \frac{ V_{out}}{ V_{in} } I_{out} = \frac{ d }{ 1 - d } I_{out}
    \end{align*}
  \end{block}
  \begin{important}{Input current}
    \begin{align}
      I_{in} = \frac{ d }{ 1 - d } I_{out}
    \end{align}
  \end{important}
  \begin{block}{Inductor current}
    \begin{align*}
      I_{L} = \frac{ 1 }{ 1 - d } I_{out} = \frac{ 1 - d + d }{ 1 - d } I_{out} = \left( 1 + \frac{ d }{ 1 - d } \right) I_{out}
    \end{align*}
  \end{block}
  \begin{important}{Inductor current}
    \begin{align}
     I_L = I_{in} + I_{out}
    \end{align}
  \end{important}
\end{frame}

\begin{frame}
  \frametitle{Buckboost - all operating modes}
  \begin{columns}[T]
    \column{.31\framewidth}
    \begin{block}{Continuous Conduction Mode (CCM)}
      \input{tikz/cont_cond_mode.tikz.tex}
    \end{block}
    \column{.31\framewidth}
    \begin{block}{Boundary Conduction Mode (BCM)}
      \input{tikz/bound_cond_mode.tikz.tex}
      also called Critical Conduction Mode (CrCM) (sometimes abbreviated CCM)
    \end{block}
    \column{.31\framewidth}
    \begin{block}{Discontinuous Conduction Mode (DCM)}
      \input{tikz/discont_cond_mode.tikz.tex}
      analysis above not valid here! (later in course!)
    \end{block}
  \end{columns}
\end{frame}

%%% Local Variables:
%%% TeX-master: "../presentation.tex"
%%% End: