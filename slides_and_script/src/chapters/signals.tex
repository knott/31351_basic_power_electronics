\section<presentation>[Signals]{Signals}
\section<article>{Signals}
\label{sec:signals}

% \begin{frame}
%   \sectionpage
%   \tableofcontents[hideothersubsections]
% \end{frame}

\subsection<presentation>[Defs]{Definitions}
\subsection<article>{Definitions}
\label{sub:defs}

\begin{frame}
  \frametitle{Definitions}
  \begin{block}{Conditions}
    For periodic, i.e. time-repetititive signals only!
  \end{block}
  \begin{important}{Average Value}
    \centering
    $V_{avg} = \frac{1}{T} \int_0^T v(t) dt$
  \end{important}
  \begin{important}{Root-Mean-Square (RMS) Value}
    \centering
    $V_{RMS} = \sqrt{ \frac{1}{T} \int_0^T v^2(t) dt}$
  \end{important}
  \begin{important}{Crest Factor}
    \centering
    $\xi = \frac{ \hat{ V } }{ V_{RMS} }$
  \end{important}
  \only<article>{Note, that \idx{average value}, \idx{DC value} and \idx{mean} value are the same thing and different notations occur: $V_{DC} = V_{avg} = V = \overline{v}$.}
\end{frame}

\begin{frame}
  \frametitle {Squarewave}
  \begin{exercise}{Squarewave with duty cycle $d$}
    \input{tikz/squarewave.tikz.tex}\\
    What is the mean value $V_{avg}$, the RMS value $V_{RMS}$ and the crest factor $\xi$ of this repetitive signal, with $v( t ) = v( t + T )$?
  \end{exercise}
\end{frame}

\subsection<presentation>[AC \& DC]{Superpositions of AC \& DC}
\subsection<article>{Superposition of AC \& DC}
\label{sub:ac_dc}

\begin{frame}[shrink]
  \frametitle{Superposition of DC \& AC}
  \begin{derivation}{Elimination of DC by Fourier Series}
    For periodic signals $v( t ) = v( t + T )$, \newline
    $\Rightarrow$ Fourier Series can be applied:% \newline
    \begin{equation}
      \label{eq:fourier}
          v( t ) = \sum_{n=0}^{\infty} V_n \cos \left( n \omega t + \varphi_n \right)
    \end{equation}
%    $v( t ) = \sum_{n=0}^{\infty} V_n \cos \left( n \omega t + \varphi_n \right)$ \newline
    $\Rightarrow$ Elimination of DC:
    \begin{equation}
      \label{eq:eliminationDC}
      v( t ) = V_{DC} + \underbrace{ \sum_{n=1}^{\infty} V_n \cos \left( n \omega t + \varphi_n \right) }_{v_{AC} ( t )}
    \end{equation}
    \begin{equation}
      \label{eq:superposDCAC}
      \Rightarrow v( t ) = V_{DC} + v_{AC} ( t )
    \end{equation}
    Calculating the RMS value of this signal $\rightarrow$ the periodicity sine waves $\rightarrow$ integrals over a whole period of a sine wave is always zero.
  \end{derivation}
\end{frame}

\begin{frame}
  \frametitle{RMS value of signals containing both DC and AC}
  \begin{important}{}
    \begin{equation}
      \label{eq:RMS_DCAC}
      V_{RMS} = \sqrt{ V_{DC}^2 + V_{AC_{RMS}} ^2 }
    \end{equation}
  \end{important}
 \begin{block}{Naming}
   The DC value is also called average, mean, offset or bias point. \newline
   The remaining time dependent part of a signal is often called ripple or AC part.
 \end{block}
\end{frame}

\begin{frame}
 \frametitle{Often occurring waveforms}
 \begin{columns}[T]
   \column{.48\framewidth}
   \begin{block}{a) Triangular ripple}
     \input{tikz/triangular_ripple.tikz.tex}
     \vspace{2mm}
     \begin{equation*}
       \label{eq:triripple}
       I_{RMS} = \sqrt{ I^2 + \frac{ \Delta I^2 }{ 3 } }
     \end{equation*}
   \end{block}
   \column{.48\framewidth}
   \begin{block}{b) Puls with ripple}
     \input{tikz/pulse_with_ripple.tikz.tex}
     \begin{equation*}
       \label{eq:pulsripple}
       I_{RMS} = \sqrt{ d \left( I^2 + \frac{ \Delta I^2 }{ 3 } \right) }
     \end{equation*}
   \end{block}
 \end{columns}
 \uncover<2->
 {
   \begin{exercise}{DC Values}
     What are the DC values the above repetitive signals?
   \end{exercise}
 }
\end{frame}

\begin{frame}
  \frametitle{Exercises}
  What are the mean values and the RMS values of the following repetitive signals?
  \begin{exercise}{Halfwave sawtooth}
    \input{tikz/sawtooth_halfwave.tikz.tex}
  \end{exercise}
\end{frame}

\begin{frame}
  \frametitle{Exercises}
    \begin{exercise}{Sine wave $v(t) = \hat{V} \sin ( \omega t )$}
      \input{tikz/sinewave.tikz.tex}
    \end{exercise}
\end{frame}

\begin{frame}
  \frametitle{Exercises}
  \begin{exercise}{More Triangles, ...}
    \input{tikz/triangle.tikz.tex}
  \end{exercise}
\end{frame}

\begin{frame}
  \frametitle{Exercises}
  \begin{exercise}{... another square wave, ...}
    \input{tikz/squarewave_ac.tikz.tex}
  \end{exercise}
\end{frame}

\begin{frame}
  \frametitle{Exercises}
  \begin{exercise}{... two triangles, ...}
    \input{tikz/triangle_rectified.tikz.tex}
  \end{exercise}
\end{frame}

\begin{frame}
  \frametitle{Exercises}
  \begin{exercise}{... a sawtooth, ...}
    \input{tikz/sawtooth.tikz.tex}
  \end{exercise}
\end{frame}

\begin{frame}
  \frametitle{Exercises}
  \begin{exercise}{... and two sawteeth.}
    \input{tikz/sawtooth_double_frequency.tikz.tex}
  \end{exercise}
\end{frame}

\subsection<presentation>[Power Factor]{Power Factor}
\subsection<article>{Power Factor}
\label{sub:pwrfactor}

\begin{frame}
  \frametitle{Power transfer}
  \begin{block}{AC power source and load}
    \input{tikz/source_and_load.tikz.tex}
  \end{block}
  \begin{block}{Grid voltage and load current}
    \begin{align}
      \label{eq:gridVI}
      v( t ) &=  \hat{V} \sin \left( \omega t + \varphi_V \right) \\
      i( t ) &= \hat{I} \sin \left( \omega t + \varphi_I \right)
    \end{align}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Instantaneous power}
  \begin{derivation}{as product of voltage and current}
    \begin{equation}
      \begin{aligned}
      p( t ) &= v( t ) i( t )\\
             &= \ldots\\
             &= \frac{ \hat{ V } \hat{ I } }{ 2 } \left[ \left\{ \overbrace{ 1 - \cos \left( 2 \omega t + 2 \varphi_V \right) }^{\textcolor{dtured}{A}} \right\} \cos( \varphi_V - \varphi_I ) \right.\\
             & ~ ~ ~ \left.  - \underbrace{ \sin \left( 2 \omega t + 2 \varphi_V \right) }_{\textcolor{dtudarkgreen}{B}} \sin( \varphi_V - \varphi_I ) \right]
        \end{aligned}
     \end{equation}
  \end{derivation}
\end{frame}

\begin{frame}
  \frametitle{Partial signals \textcolor{dtured}{A} and \textcolor{dtudarkgreen}{B}}
    \begin{block}{look like this:}
      \input{tikz/partial_signals_power.tikz.tex}
    \end{block}
    \begin{block}{Deduction}
      \begin{itemize}
        \item \textcolor{dtured}{A} is always positive
        \item \textcolor{dtudarkgreen}{B} has no DC part
      \end{itemize}
    \end{block}
\end{frame}

\begin{frame}[shrink]
  \frametitle{Power definitions}
  \begin{block}{therefore}
    \begin{equation}
      \begin{aligned}
        P &= \frac{ \hat{ V } \hat{ I } }{ 2 } \left( 1 - \cos \left( 2 \omega t + 2 \varphi_V \right) \right) \cos( \varphi_V - \varphi_I )\\
        Q & = - \frac{ \hat{ V } \hat{ I } }{ 2 } \sin \left( 2 \omega t + 2 \varphi_V \right) \sin( \varphi_V - \varphi_I )
        \end{aligned}
     \end{equation}
  \end{block}
  \begin{important}{different parts of power:}
    \begin{itemize}
     \item $P$ is called active power or real power,
     \item $Q$ is called reactive power and
     \item $\underline{S} = P + jQ$ is called complex power
    \end{itemize}
  \end{important}
  \only<article>{\idx{active power} $=$\idx{real power} P, \idx{reactive power} Q and \idx{complex power} Q.}
  \begin{important}{power factor}
    \begin{equation}
      \label{eq:pwrfact}
       \lambda = \frac{P}{S}
    \end{equation}
  \end{important}
\end{frame}

\subsection<presentation>[Lab Signals]{Laboratory Exercises for Signals}
\subsection<article>{Laboratory Exercises for Signals}
\label{sub:labsignals}

\begin{frame}
  \frametitle{Function generator}
  \begin{handson}{Get familiar with the function generator}
    Turn on and play with it:
    \vspace{-0.3cm}
    \begin{wrapfigure}{r}{0.75\framewidth}
      \includegraphics[width=0.8\linewidth]{pics/33500B.jpg} 
    \end{wrapfigure}
    \vspace{-0.2em}
    \begin{itemize}
      \item different waveforms,
      \item amplitude,
      \item offset,
      \item frequency.
      \end{itemize}
      \ \\[2em]
  \end{handson}
\end{frame}

\begin{frame}
  \frametitle{Oscilloscope}
  \begin{handson}{Get familiar with the oscilloscope}
    Turn on and play with it:
    \begin{wrapfigure}{r}{0.6\framewidth}
      \vspace{-0.5cm}
      \includegraphics[width=0.9\linewidth]{pics/MSOX3024T.jpg}
    \end{wrapfigure}
    \vspace{-1em}
    \begin{itemize}
      \item channel settings,
      \item trigger settings,
      \item cursor,
      \item measurement settings,
      \item saving data to disk,
      \item taking screenshots.
    \end{itemize}
  \end{handson}
\end{frame}

\begin{frame}
  \frametitle{Experimental verifying}
  \begin{handson}{of waveforms}
    Verify the calculations of the waveforms from exercises 2.1, 2.4, 2.5, 2.6, 2.7, 2.8, and 2.9 with the waveform generator and the oscilloscope.
  \end{handson}
  \begin{handson}{bonus task:}
    Verify the calculations of the  remaining waveforms 2.2, and 2.3, with the arbitrary waveform generator function in the waveform generator and the oscilloscope.
  \end{handson}
\end{frame}

%%% Local Variables:
%%% TeX-master: "../presentation.tex"
%%% End: