\section<presentation>[Parameters]{Parameters of Converters}
\section<article>{Parameters of Converters}
\label{sec:parameters}

% \begin{frame}
%   \sectionpage
%   \tableofcontents[hideothersubsections]
% \end{frame}

\subsection[DCM]{Discontinuous Conduction Mode}
\label{sub:DCM}

\begin{frame}
  \frametitle{Discontinuous Conduction Mode}
  \colspace
  \begin{columns}[T]
      \column{0.58\framewidth}
      \begin{block}{Inductor current - waveform}
        \centering
        \input{tikz/current_inductor_DCM.tikz.tex}
      \end{block}
      \column{0.38\framewidth}
      \begin{block}{Remaining pre- conditions in DCM}
        \begin{enumerate}
          \item \label{enum:eta100} Ideal power converters are $100~\%$ efficient $\Rightarrow P_{in} = P_{out}$ \newline $\Rightarrow V_{out} = \frac{I_{in}}{I_{out}} V_{in}$.
          \item \label{enum:ohmslaw} Ohms law across the load resistor: $I_{out_{avg}} = \frac{V_{out}}{R_L}$.
        \end{enumerate}
      \end{block}
    \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Buck in discontinuous conduction mode}
  \colspace
  \begin{columns}[T]
    \column{0.53\framewidth}
    \begin{block}{Voltage \& current across \& through inductor}
      \input{tikz/buck_DCM.tikz.tex}
    \end{block}
    \column{0.43\framewidth}
    \begin{derivation}{\newline Input current}
      \begin{enumerate}
        \setcounter{enumi}{2}
        \item peak input current:
      \uncover<1->
      {
        \begin{align*}
          I_{in_{peak}} = \frac{V_{in} - V_{out}}{L} dT
        \end{align*}
      \item \label{enum:avg_cur_in} mean input current:
      }
      \uncover<2->
      {
        \begin{align*}
          I_{in_{avg}} = \frac{V_{in} - V_{out}}{2L} d^2T
        \end{align*}
      }
     \end{enumerate}
    \end{derivation}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Buck in discontinuous conduction mode}
  \begin{derivation}{DC transfer function}
    Putting \ref{enum:avg_cur_in} and \ref{enum:ohmslaw} into \ref{enum:eta100}:
    \begin{align*}
      V_{out}= \frac{V_{in} - V_{out}}{V_{out}} \underbrace{\frac{R}{2L} d^2 T}_{\alpha} V_{in}\\
      V_{out}^2 + \alpha V_{in} V_{out} - \alpha V_{in}^2 = 0\\
      V_{out} = \frac{-\alpha V_{in} \pm \sqrt{ \alpha^2 V_{in}^2 + 4 \alpha V_{in}^2}}{2}
    \end{align*}
  \end{derivation}
   \begin{important}{DC transfer function of buck in discontinuous conduction mode}
     \begin{align}
        V_{out} = \frac{V_{in}}{2} \left( \sqrt{\alpha^2 + 4 \alpha} - \alpha \right)
     \end{align}
   \end{important}
\end{frame}

\begin{frame}
  \frametitle{Boost in discont. conduction mode}
  \colspace
  \begin{columns}[T]
    \column{0.53\framewidth}
    \begin{block}{Voltage \& current across \& through inductor}
      \input{tikz/boost_DCM.tikz.tex}
    \end{block}
    \column{0.43\framewidth}
    \begin{derivation}{\newline Input current}
      \begin{enumerate}
        \setcounter{enumi}{2}
      \item peak input current:
        \vspace{-1em}
      \uncover<1->
      {
        \begin{align*}
          I_{in_{peak}} = \frac{V_{in}}{L} dT
        \end{align*}
        \vspace{-2em}
      \item \label{enum:boost_avg_cur_in} mean input current:
      }
      \uncover<2->
      {
        \vspace{-1ex}
        \begin{align*}
          I_{in_{avg}} = \frac{V_{in}}{2L} d \left( d + m \right) T
        \end{align*}
%        \vspace{-1em}
      }
     \uncover<3->
     {
       \hspace{-1em} compared to buck, one more unknown: $m$, therefore one relation needed to solve the set of equations
     }
    \end{enumerate}
   \end{derivation}
 \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Boost in discont. conduction mode}
  \begin{derivation}{Average output current in boost}
    Average output current is known from \ref{enum:ohmslaw}, but can also be expressed from average diode current:
    \uncover<1->
    {
      \begin{enumerate}
        \setcounter{enumi}{4}
      \item \label{enum:avg_cur_out} average output current:
        \begin{align*}
          I_{out_{avg}} = \frac{m}{2} I_{in_{peak}} = \frac{V_{in}}{2 L} m d T
        \end{align*}
      \end{enumerate}
    }
    \uncover<2->
    {
      solving for $m$ and using \ref{enum:ohmslaw} as the other relation for the average output current gives:
      \begin{align*}
        m = \frac{ 2 L }{ R } \frac{ 1 }{ d T} \frac{ V_{out}}{ V_{in}}
      \end{align*}
    }
  \end{derivation}
\end{frame}

\begin{frame}
  \frametitle{Boost in discont. conduction mode}
    \begin{derivation}{DC transfer function}
      Now putting $m$ into \ref{enum:avg_cur_in} and using the result together with \ref{enum:ohmslaw} in \ref{enum:eta100} gives:
%      \vspace{-1em}
    \begin{align*}
      V_{out}= \frac{\frac{V_{in}}{2 L} d \left( d + \frac{2 L}{ R } \frac{1}{ d T } \frac{V_{out}}{V_{in}} \right) T }{V_{out}} R V_{in}\\
      V_{out}^2 =\underbrace{\frac{R}{2L} d^2 T}_{\alpha} V_{in}^2 + V_{in} V_{out}\\
      V_{out}^2 - V_{in} V_{out} - \alpha V_{in}^2 = 0\\
      V_{out} = \frac{V_{in} \pm \sqrt{V_{in}^2 + 4 \alpha V_{in}^2}}{2}
    \end{align*}
  \end{derivation}
   \begin{important}{DC transfer function of boost in DCM}
     \begin{align}
        V_{out} = \frac{V_{in}}{2} \left( 1 + \sqrt{1 + 4 \alpha} \right)
     \end{align}
   \end{important}
\end{frame}

\begin{frame}
  \frametitle{Buck-boost in DCM}
  \colspace
  \begin{columns}[T]
    \column{0.53\framewidth}
    \begin{block}{Voltage \& current across \& through inductor}
      \input{tikz/buckboost_DCM.tikz.tex}
    \end{block}
    \column{0.43\framewidth}
    \begin{derivation}{\newline Input current}
      \begin{enumerate}
        \setcounter{enumi}{2}
      \item peak input current:
        \vspace{-1em}
      \uncover<1->
      {
        \begin{align*}
          I_{in_{peak}} = \frac{V_{in}}{L} dT
        \end{align*}
        \vspace{-2em}
      \item \label{enum:bb_avg_cur_in} mean input current:
      }
      \uncover<2->
      {
        \vspace{-1ex}
        \begin{align*}
          I_{in_{avg}} = \frac{V_{in}}{2L} d^2 T
        \end{align*}
%        \vspace{-1em}
      }
    \end{enumerate}
   \end{derivation}
 \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Buck-boost in DCM}
  \begin{derivation}{DC transfer function}
    Putting \ref{enum:bb_avg_cur_in} and \ref{enum:ohmslaw} into \ref{enum:eta100}:
    \begin{align*}
      V_{out}= \frac{V_{in}^2 R}{2 L V_{out}} d^2 T
    \end{align*}
  \end{derivation}
   \begin{important}{DC transfer function of buck-boost in discontinuous conduction mode}
     \begin{align}
              V_{out} = d V_{in} \sqrt{\frac{ R T }{ 2 L} }
     \end{align}
   \end{important}
\end{frame}

\subsection[Synchr Rect]{Synchronous Rectification}
\label{sub:syncrec}

\begin{frame}
  \frametitle{Synchronous rectification}
  \begin{block}{Replacement of diode with MOSFET}
    \centering
    \input{tikz/diode_to_FET.tikz.tex}
  \end{block}
  % \vspace{-1em}
  \colspace
  \begin{columns}[T]
    \column{0.48\framewidth}
    \begin{block}{Advantages}
      \begin{itemize}
      \item lower conduction losses
      \item enables bipolar output, e.g. for motor drives or amplifiers
      \end{itemize}
    \end{block}
    \column{0.48\framewidth}
    \begin{block}{Disadvantages}
      \begin{itemize}
      \item needs an extra gate driver, i.e. more gate drive losses
      \item potentially complex control as it is always in continuous condcution mode
      \end{itemize}
    \end{block}
  \end{columns}
\end{frame}

\subsection[Interleave]{Interleave}
\label{sub:interleave}

\begin{frame}
  \frametitle{Interleave}
  \colspace
  \begin{columns}[T]
    \column{0.63\framewidth}
    \begin{block}{Interleaved buck}
      \uncover<1->{}
      \input{tikz/interleave_buck.tikz.tex}
    \end{block}
    \column{0.33\framewidth}
    \uncover<1->
    {
      \begin{block}{Gate voltages}
        \input{tikz/interleave_buck_gates.tikz.tex}
      \end{block}
    }
    \uncover<2->
    {
    \begin{block}{Currents $d=0.5$}
      \input{tikz/interleave_buck_currents.tikz.tex}
    \end{block}
  }
  \uncover<3->{}
  \end{columns}
\end{frame}

\begin{frame}[shrink=4]
  \frametitle{Buck in various conduction modes}
  \begin{exercise}{Buck}
    An ideal buck converter provides $V_{out} = 5~\si{\volt}$ from $V_{in} = 12~\si{\volt}$ while it operates with a period of $T = 10~\si{\micro\second}$. The inductor is $L = 58.1~\si{\micro\henry}$, while the output capacitor is assumed to be infinite.
    \begin{itemize}
      \item Plot the inductor current for a load current $I_{out} = 5~\si{\ampere}$.
      \item The output current is reduced to the point, where the inductor current hits zero exactly at the end of the period. Draw the inductor current for this constelation.
        \item How big is the load resistance in this case?
      \item The load resistance is increased further: now the peak current of the inductor is $\hat{I}_L = 354~\si{\milli\ampere}$. How long does the transistor conduct? Draw the inductor current.
      \item How big is the average inductor current and load resistance now?
      \end{itemize}
  \end{exercise}
\end{frame}

\begin{frame}%[shrink=1]
  \frametitle{Boost}
  \begin{exercise}{Boost in BCM...}
        The diagram shows the current waveform of an ideal boost, powered by a constant DC voltage $V_{in} = 180~\si{\volt}$ with infinite output capacitance and finite inductance $L_1$.%\newline
    \begin{wrapfigure}[5]{r}{0.4\framewidth}
      \vspace{-1em}
      \hspace{-2em}
      \input{tikz/current_BCM.tikz.tex}
    \end{wrapfigure}
    \vspace{-1em}
    \begin{enumerate}[a]
      \item What is the inductance $L_1$?
      \item What is the output voltage?
      \item What is the load resistance?
      \item Now the output power shall be doubled but the converter shall maintain BCM. Therefore a new inductor $L_a$ is designed using the same core material and size as $L_1$. Therefore $L_a = \frac{L_1}{2}$. How does the number of turns change from $L_1$ to $L_a$?
    \end{enumerate}
  \end{exercise}
\end{frame}

\begin{frame}%[shrink=5]
  \frametitle{Boost}
  \addtocounter{exercise}{-1}
  \begin{exercise}{...and interleaved boost}
    The inductance calculated in a) is used for $L_2$ \& $L_3$ in an interleaved boost:
    % \begin{wrapfigure}{l}{0.8\framewidth}
    \input{tikz/interleave_boost.tikz.tex}
    \begin{enumerate}[a]
      \setcounter{enumi}{4}
      \item Draw the input current $I_{in}$ of the converter and label its maximum and minimum values.
      \item What is the RMS-value of the currents in the MOSFETs?
    \end{enumerate}
    % \end{wrapfigure}
  \end{exercise}
\end{frame}

%%% Local Variables:
%%% TeX-master: "../presentation.tex"
%%% End: