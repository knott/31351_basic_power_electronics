.PS
# Test.m4
cct_init
log_init
# Use this file to do a quick test of diagrams you are developing.
# Enter the drawing commands here and (if you have a "make" utility) type
#   make tst
# Otherwise, to process a file called filename.m4, type one of
#   make filename.ps
#   make filename.eps
#   make filename.pdf
#   make filename.gif (requires ImageMagick convert)
#   make filename.png (requires ImageMagick convert)
#   make filename.tif (requires ImageMagick convert)
# To perform tests in a new folder, copy the Makefile, this file,
#   and tst.tex (or your own equivalent) to that folder.
# cct_init                       # Read in macro definitions and set defaults
# elen = 0.75                    # Variables are allowed; default units are inches
##### allowing nodes to be refered by tikz later
### from personal e-mail correspondance with D. Aplevich in 2017:
# dnl                      ` namedstr(name,string,location)'
# define(`namedstr',`command sprintf("\draw (%g,%g) node(`$1'){`$2'};",(`$3').x,(`$3').y)')
dnl	`tikznode(name,location,string)'
define(`tikznode', `command sprintf("\draw (%g,%g) node [inner sep=0pt, minimum size=0pt] (`$1'){`$3'};", (`$2').x/scale, (`$2').y/scale)')
scale = 2.54		       # cm
elen = 2
#linethick = 2
linethick_(2.0)
Origin: Here                   # Position names are capitalized

	line right_ 0.25*elen_
	dot
	{
IND:		inductor(right_ 0.75*elen_, W)
RES: 		resistor(right_ 0.75*elen_, , E)
		dot
		line right_ 0.25*elen_
	}
	line down_ 0.35*elen_
	corner
CAP:	capacitor( right_ 1.5*elen_ )
	corner
	line to RES.end

	tikznode(IND_w,IND.w)
	tikznode(IND_e,IND.e)
.PE
