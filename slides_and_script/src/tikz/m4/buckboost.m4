.PS
cct_init
log_init
##### allowing nodes to be refered by tikz later
### from personal e-mail correspondance with D. Aplevich in 2017:
dnl	`tikznode(name,location,string)'
define(`tikznode', `command sprintf("\draw (%g,%g) node [inner sep=0pt, minimum size=0pt] (`$1'){`$3'};", (`$2').x/scale, (`$2').y/scale)')
dnl	`tikzcoordinate(name,location)'
define(`tikzcoordinate', `command sprintf("\draw (%g,%g) node [shape=coordinate] (`$1'){};", (`$2').x/scale, (`$2').y/scale)')
scale = 2.54		       # cm
elen = 2
#define(‘dimen_’,(dimen_*2))
#linethick = 2
linethick_(2.0)
arrowht = 0.25
#dimen_ = 3
Origin: Here                   # Position names are capitalized

#line right_ 0.4*elen_

SRC:  source( up_ elen_, V, )

      move to SRC.s
      corner
      line right_ elen_
      dot
      line right_ elen_
      dot
      line right_ elen_
      b_current( I_{out}, , O, E, )
      corner
LOAD: resistor( up_ elen_, E ); rlabel( , R_L )
      corner
      line left_ elen_
      dot
      {
CAP:	capacitor( down_ elen_ ); rlabel( , C )
	dot
      }
DIODE:  diode( left_ elen_ )
      dot
      {
IND:	inductor( down_ elen_, L, ); llabel( , L )
      	dot
      	b_current( I_{L} )
      }
SW:   switch( left_ elen_ )
      corner

      tikzcoordinate( SRC_s, SRC.s )
      tikzcoordinate( SRC_n, SRC.n )
      tikzcoordinate( IND_n, IND.n )
      tikzcoordinate( IND_s, IND.s )
      tikzcoordinate( CAP_s, CAP.s )
      tikzcoordinate( CAP_n, CAP.n )
      tikzcoordinate( LOAD_s, LOAD.s )
      tikzcoordinate( LOAD_n, LOAD.n )

.PE
