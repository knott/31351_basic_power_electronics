.PS
cct_init
log_init
##### allowing nodes to be refered by tikz later
### from personal e-mail correspondance with D. Aplevich in 2017:
dnl	`tikznode(name,location,string)'
define(`tikznode', `command sprintf("\draw (%g,%g) node [inner sep=0pt, minimum size=0pt] (`$1'){`$3'};", (`$2').x/scale, (`$2').y/scale)')
dnl	`tikzcoordinate(name,location)'
define(`tikzcoordinate', `command sprintf("\draw (%g,%g) node [shape=coordinate] (`$1'){};", (`$2').x/scale, (`$2').y/scale)')
scale = 2.54		       # cm
elen = 2
#define(‘dimen_’,(dimen_*2))
#linethick = 2
linethick_(2.0)
arrowht = 0.25
#dimen_ = 3
### define DTU colors ###
# primary colors #
define( `dtured', `0.6, 0, 0' )
define( `dtugrey', `0.6 ,0.6, 0.6' )
# secondary colors #
define( `dtuyellow', `1.00, 0.80, 0.00' )
define( `dtuorange', `1.00, 0.60, 0.00' )
define( `dtulightred', `1.00, 0.00, 0.00' )
define( `dtubrown', `0.60, 0.00, 0.00' )
define( `dtupurple', `0.80, 0.20, 0.60' )
define( `dtuviolet', `0.40, 0.00, 0.60' )
define( `dtudarkblue', `0.20, 0.40, 0.80' )
define( `dtulightblue', `0.20, 0.80, 1.00' )
define( `dtulightgreen', `0.60, 0.80, 0.20' )
define( `dtudarkgreen', `0.40, 0.80, 0.00' )
define( `dtucoolgrey', `0.59, 0.58, 0.57' )
Origin: Here                   # Position names are capitalized

SRC:  source( up_ elen_, V, )
      b_current( I_{in}, ,O,E,0.2 )
      dot
      {
	inductor( right_ elen_ , W, ); llabel( , L_3 )
	dot
	{
SW3:		reversed( `switch', down_ elen_ )
		dot
	}
	line right_ elen_
D3:	diode( right_ elen_ )
	dot
	line up_ elen_
      }
      line up_ elen_
      corner
      inductor( right_ elen_ , W, ); llabel( , L_2 )
      line right_ elen_
      dot
      {
	line down_ elen_
SW2:	reversed( `switch', down_ elen_ )
	dot
      }
D2:   diode( right_ elen_ )
      dot
      line right_ elen_
      corner
LOAD: resistor( down_ 2*elen_, E ); llabel( , R_L )
      corner
      line to SRC.s
      corner

      move to D3.e
CAP:  capacitor( down_ elen_ ); rlabel( , C )
      dot


      tikzcoordinate( SW2, SW2 )
      tikzcoordinate( SW3, SW3 )

.PE
