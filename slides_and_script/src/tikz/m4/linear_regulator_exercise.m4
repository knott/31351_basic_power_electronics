.PS
# Test.m4
cct_init
log_init
# Use this file to do a quick test of diagrams you are developing.
# Enter the drawing commands here and (if you have a "make" utility) type
#   make tst
# Otherwise, to process a file called filename.m4, type one of
#   make filename.ps
#   make filename.eps
#   make filename.pdf
#   make filename.gif (requires ImageMagick convert)
#   make filename.png (requires ImageMagick convert)
#   make filename.tif (requires ImageMagick convert)
# To perform tests in a new folder, copy the Makefile, this file,
#   and tst.tex (or your own equivalent) to that folder.
# cct_init                       # Read in macro definitions and set defaults
# elen = 0.75                    # Variables are allowed; default units are inches
scale = 2.54		       # cm
elen = 2
#linethick = 2
linethick_(2.0)
Origin: Here                   # Position names are capitalized
#
# AC mains, rectifier bridge, half-wave rectifier and input decoupuling capacitor:
VIN: source(up_ elen_, V); llabel(,V_{in},)
     corner
     line right_ elen_
LR:  box "linear" "regulator"
     line right_ 0.5*elen_
     line right_ 0.5*elen_
     b_current(I_{out})
     corner
LOAD: resistor(down_ elen_,E); llabel(,R_{load}); rlabel(+,V_{out},-)
     corner
     line to VIN.s
     corner

     move to LR.s
     line down_ 0.3*elen_
     line to (Here.x,VIN.s.y)
     b_current(I_{bias})
     dot

.PE
