.PS
cct_init
log_init
##### allowing nodes to be refered by tikz later
### from personal e-mail correspondance with D. Aplevich in 2017:
dnl	`tikznode(name,location,string)'
define(`tikznode', `command sprintf("\draw (%g,%g) node [inner sep=0pt, minimum size=0pt] (`$1'){`$3'};", (`$2').x/scale, (`$2').y/scale)')
dnl	`tikzcoordinate(name,location)'
define(`tikzcoordinate', `command sprintf("\draw (%g,%g) node [shape=coordinate] (`$1'){};", (`$2').x/scale, (`$2').y/scale)')
scale = 2.54		       # cm
elen = 2
#define(‘dimen_’,(dimen_*2))
#linethick = 2
linethick_(2.0)
arrowht = 0.25
#dimen_ = 3
Origin: Here                   # Position names are capitalized

#line right_ 0.4*elen_

SRC:  source( up_ elen_, V, )
      corner
IND:  inductor( right_ elen_, L, ); llabel( , L )
      dot
DIODE:  diode( right_ elen_ )
      dot
      {
CAP:	capacitor( down_ elen_ ); rlabel( , C )
	dot
      }
      line right_ elen_
      b_current( I_{out} )
      corner
LOAD: resistor( down_ elen_, E ); llabel( , R_L )
      corner
      line to (IND.e.x,Here.y)
      dot
      {
SW:   switch( up_ elen_ )
      }
      line to SRC.s
      corner

      tikzcoordinate( SRC_s, SRC.s )
      tikzcoordinate( SRC_n, SRC.n )
      tikzcoordinate( SW_n, SW.n )
      tikzcoordinate( SW_s, SW.s )
      tikzcoordinate( CAP_s, CAP.s )
      tikzcoordinate( CAP_n, CAP.n )
      tikzcoordinate( LOAD_s, LOAD.s )
      tikzcoordinate( LOAD_n, LOAD.n )

.PE
