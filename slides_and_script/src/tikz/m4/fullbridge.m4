.PS
cct_init
log_init
# central block library:
include(/Users/aknott/Documents/research/01_drawings/blocks.m4)
##### allowing nodes to be refered by tikz later
### from personal e-mail correspondance with D. Aplevich in 2017:
dnl	`tikznode(name,location,string)'
define(`tikznode', `command sprintf("\draw (%g,%g) node [inner sep=0pt, minimum size=0pt] (`$1'){`$3'};", (`$2').x/scale, (`$2').y/scale)')
dnl	`tikzcoordinate(name,location)'
define(`tikzcoordinate', `command sprintf("\draw (%g,%g) node [shape=coordinate] (`$1'){};", (`$2').x/scale, (`$2').y/scale)')
##### add uncover for animating slides with beamer
## usage:
## setuncover(start,end)
## <whatever code should be animated>
## resetuncover
## where `start' is first slide to show and `end' is last slide (if all, use `-')
dnl   `setuncover(start,end)'
define(`setuncover', `command sprintf("\uncover<$1$2>{")')
dnl   `resetuncover()'
define(`resetuncover', `command sprintf("}")')
###### same for only command:
dnl   `setonly(start,end)'
define(`setonly', `command sprintf("\only<$1$2>{")')
dnl   `resetonly()'
define(`resetonly', `command sprintf("}")')
#
scale = 2.54		       # cm
elen = 2
#define(‘dimen_’,(dimen_*2))
#linethick = 2
linethick_(2.0)
arrowht = 0.25
#dimen_ = 3
### define DTU colors ###
# primary colors #
define( `dtured', `0.6, 0, 0' )
define( `dtugrey', `0.6 ,0.6, 0.6' )
# secondary colors #
define( `dtuyellow', `1.00, 0.80, 0.00' )
define( `dtuorange', `1.00, 0.60, 0.00' )
define( `dtulightred', `1.00, 0.00, 0.00' )
define( `dtubrown', `0.60, 0.00, 0.00' )
define( `dtupurple', `0.80, 0.20, 0.60' )
define( `dtuviolet', `0.40, 0.00, 0.60' )
define( `dtudarkblue', `0.20, 0.40, 0.80' )
define( `dtulightblue', `0.20, 0.80, 1.00' )
define( `dtulightgreen', `0.60, 0.80, 0.20' )
define( `dtudarkgreen', `0.40, 0.80, 0.00' )
define( `dtucoolgrey', `0.59, 0.58, 0.57' )
Origin: Here                   # Position names are capitalized

SRC:	source( up_ 3*elen_, V, )#; llabel( , V_{in} )
	corner
	line right_ 0.75*elen_
	dot
	{
SW1:		reversed( `switch', down_ elen_ )
		dot
		line down_ elen_
SW2:		reversed( `switch', down_ elen_ )
		dot
	}
	line right_ 0.75*elen_
	corner
SW3:	reversed( `switch', down_ elen_ )
	line down_ elen_
SW4:	reversed( `switch', down_ elen_ )
	corner
	line to SRC.s
	corner

# primary side x-former:
	move to SW1.s
	line right_ 1.5*elen_
PRIM:	inductor( down_ elen_, L, ); llabel( N_1 )
	line to SW4.n
	dot

# secondary side:
	move to PRIM.s + ( 0.9*elen_, -0.5*elen_ )
SEC2:	inductor( up_ elen_, L, ); llabel( , , N_2 )
	dot
SEC1:	inductor( up_ elen_, L, ); llabel( , , N_2 )
	corner
D1:	diode( right_ 0.75*elen_ )
	dot
	{
		line down_ 2*elen_
D2:		reversed( `diode', left_ 0.75*elen_ )
		corner
	}
	line right_ 0.25* elen_
	{
DFREE:		reversed( `diode', down_ elen_ )
		dot
	}
LOUT:	inductor( right_ 0.75*elen_, L, )
	dot
	{
CAP:		capacitor( down_ elen_ )
		dot
	}
	line right_ 0.75*elen_
	corner
LOAD:	resistor( down_ elen_, , E, )
	corner
	line to SEC1.s
	
# core and winding directions:
#        move to PRIM1.s + ( 0.4*elen_, 0.3*elen_ )
#	line up_ 0.4*elen_
#	move to PRIM1.s + ( 0.5*elen_, 0.3*elen_ )
#	line up_ 0.4*elen_
	move to PRIM.n + ( 0.1*elen_, -0.3*elen_ )
	dot
	move to PRIM.s + ( 0.4*elen_, -0.2*elen_ )
	line up_ 1.4*elen_
	move to PRIM.s + ( 0.5*elen_, -0.2*elen_ )
	line up_ 1.4*elen_
	move to SEC1.n + ( -0.1*elen_, -0.3*elen_ )
	dot
	move to SEC2.n + ( -0.1*elen_, -0.3*elen_ )
	dot

	tikzcoordinate( SRC, SRC )
	tikzcoordinate( SRC_n, SRC.n )
	tikzcoordinate( SRC_s, SRC.s )
	tikzcoordinate( PRIM_n, PRIM.n )
	tikzcoordinate( PRIM_s, PRIM.s )
	tikzcoordinate( SW1_n, SW1.n )
	tikzcoordinate( SW1_s, SW1.s )
	tikzcoordinate( SW2_n, SW2.n )
	tikzcoordinate( SW2_s, SW2.s )
	tikzcoordinate( SW3_n, SW3.n )
	tikzcoordinate( SW3_s, SW3.s )
	tikzcoordinate( SW4_n, SW4.n )
	tikzcoordinate( SW4_s, SW4.s )
	tikzcoordinate( SEC1_n, SEC1.n )
	tikzcoordinate( SEC1_s, SEC1.s )
	tikzcoordinate( SEC2_n, SEC2.n )
	tikzcoordinate( SEC2_s, SEC2.s )
	tikzcoordinate( D1_w, D1.w )
	tikzcoordinate( D1_e, D1.e )
	tikzcoordinate( D2_w, D2.w )
	tikzcoordinate( D2_e, D2.e )
	tikzcoordinate( DFREE_n, DFREE.n )
	tikzcoordinate( DFREE_s, DFREE.s )
	tikzcoordinate( CAP_n, CAP.n )
	tikzcoordinate( CAP_s, CAP.s )
	tikzcoordinate( LOAD_n, LOAD.n )
	tikzcoordinate( LOAD_s, LOAD.s )

.PE
