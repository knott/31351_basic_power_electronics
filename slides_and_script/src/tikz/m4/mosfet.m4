.PS
cct_init
log_init
##### allowing nodes to be refered by tikz later
### from personal e-mail correspondance with D. Aplevich in 2017:
dnl	`tikznode(name,location,string)'
define(`tikznode', `command sprintf("\draw (%g,%g) node [inner sep=0pt, minimum size=0pt] (`$1'){`$3'};", (`$2').x/scale, (`$2').y/scale)')
scale = 2.54		       # cm
elen = 2
#define(‘dimen_’,(dimen_*2))
#linethick = 2
linethick_(2.0)
#dimen_ = 2
arrowht = 0.25
Origin: Here                   # Position names are capitalized


	line up_ 0.3*elen_
	tikznode( FET_S, Here )
FET:	e_fet( up_ elen_ ) with .S at Here
#	e_fet( up_ elen_ )
#rlabel( -, V_{ds}, + )
#line right_ 0.5*elen_
      move to FET.D
      tikznode( FET_D, Here )
      line up_ 0.3*elen_
      b_current( \textcolor{dtugrey}{I_d}, , O, , 0.2 )
      line up_ 0.3*elen_

      move to FET.G
      tikznode( FET_G, Here )
      line left_ 0.3*elen_
      line left_ 0.3*elen_
      b_current( \textcolor{dtugrey}{I_g}, below_ , O, )
      


#      move to FET.G
#      b_current( I_G )

.PE
