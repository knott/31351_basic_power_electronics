.PS
cct_init
log_init
# central block library:
include(/Users/aknott/Documents/research/01_drawings/blocks.m4)
##### allowing nodes to be refered by tikz later
### from personal e-mail correspondance with D. Aplevich in 2017:
dnl	`tikznode(name,location,string)'
define(`tikznode', `command sprintf("\draw (%g,%g) node [inner sep=0pt, minimum size=0pt] (`$1'){`$3'};", (`$2').x/scale, (`$2').y/scale)')
dnl	`tikzcoordinate(name,location)'
define(`tikzcoordinate', `command sprintf("\draw (%g,%g) node [shape=coordinate] (`$1'){};", (`$2').x/scale, (`$2').y/scale)')
##### add uncover for animating slides with beamer
## usage:
## setuncover(start,end)
## <whatever code should be animated>
## resetuncover
## where `start' is first slide to show and `end' is last slide (if all, use `-')
dnl   `setuncover(start,end)'
define(`setuncover', `command sprintf("\uncover<$1$2>{")')
dnl   `resetuncover()'
define(`resetuncover', `command sprintf("}")')
###### same for only command:
dnl   `setonly(start,end)'
define(`setonly', `command sprintf("\only<$1$2>{")')
dnl   `resetonly()'
define(`resetonly', `command sprintf("}")')
#
scale = 2.54		       # cm
elen = 2
#define(‘dimen_’,(dimen_*2))
#linethick = 2
linethick_(2.0)
arrowht = 0.25
#dimen_ = 3
### define DTU colors ###
# primary colors #
define( `dtured', `0.6, 0, 0' )
define( `dtugrey', `0.6 ,0.6, 0.6' )
# secondary colors #
define( `dtuyellow', `1.00, 0.80, 0.00' )
define( `dtuorange', `1.00, 0.60, 0.00' )
define( `dtulightred', `1.00, 0.00, 0.00' )
define( `dtubrown', `0.60, 0.00, 0.00' )
define( `dtupurple', `0.80, 0.20, 0.60' )
define( `dtuviolet', `0.40, 0.00, 0.60' )
define( `dtudarkblue', `0.20, 0.40, 0.80' )
define( `dtulightblue', `0.20, 0.80, 1.00' )
define( `dtulightgreen', `0.60, 0.80, 0.20' )
define( `dtudarkgreen', `0.40, 0.80, 0.00' )
define( `dtucoolgrey', `0.59, 0.58, 0.57' )
Origin: Here                   # Position names are capitalized

SRC:	source( up_ 1.75*elen_, V, )#; llabel( , V_{in} )
	corner
	line right_ 1.5*elen_
	dot
PRIM2:	inductor( down_ elen_, L, ); llabel( N_1 )
SW2:	reversed( `switch', down_ 0.75*elen_ )
	corner
	line left_ 0.75*elen_
	dot
	{
SW1:		switch( up_ 0.75*elen_ )
		line up_ 2*elen_
		corner
		line right_ 0.75*elen_
		corner
PRIM1:		inductor( down_ elen_, L, ); llabel( N_1 )
	}
	line to SRC.s
	corner


	move to PRIM2.s + ( 0.9*elen_, 0 )
SEC2:	inductor( up_ elen_, L, ); llabel( , , N_2 )
	dot
SEC1:	inductor( up_ elen_, L, ); llabel( , , N_2 )
	corner
	line right_ 0.75*elen_
	dot
D1:	diode( up_ 0.75*elen_ )
	corner
	line right_ 0.5*elen_
LOUT1:	inductor( right_ elen_, L, )
	dot
	{
CAP1:		capacitor( down_ 1.75*elen_ )
		dot
		{
			line to SEC1.s
		}
CAP2:		capacitor( down_ 1.75*elen_ )
		dot
LOUT2:		reversed( `inductor', left_ elen_, L, )
		dot
		{
D4:			diode( up_ 0.75*elen_ )
			dot
			{
				line to SEC2.s
				corner
			}
			line to (Here.x, SEC1.n.y)
D3:			diode( up_ 0.75*elen_ )
			dot
		}
		line to (D1.x,Here.y)
		corner
D2:		diode( up_ 0.75*elen_ )
		line to D1.s
	}

# adding the amplifier:
  	move to CAP1.s
	line right_ 0.5*elen_
AMP:	amplifier( 0.5*elen_ )
	move to AMP.n
LOADP:	line to ( Here.x, LOUT1.y )
	corner
	line to LOUT1.e
	move to AMP.s
LOADN:	line to ( Here.x, LOUT2.y )
	corner
	line to LOUT2.e
	move to AMP.e
	line right_ 0.3*elen_
	speaker( 0.5*elen_ )

# core and winding directions:
#        move to PRIM1.s + ( 0.4*elen_, 0.3*elen_ )
#	line up_ 0.4*elen_
#	move to PRIM1.s + ( 0.5*elen_, 0.3*elen_ )
#	line up_ 0.4*elen_
	move to PRIM1.n + ( 0.1*elen_, -0.3*elen_ )
	dot
	move to PRIM2.s + ( 0.4*elen_, 0.3*elen_ )
	line up_ 1.4*elen_
	move to PRIM2.s + ( 0.5*elen_, 0.3*elen_ )
	line up_ 1.4*elen_
	move to PRIM2.n + ( 0.1*elen_, -0.3*elen_ )
	dot
	move to SEC1.n + ( -0.1*elen_, -0.3*elen_ )
	dot
	move to SEC2.n + ( -0.1*elen_, -0.3*elen_ )
	dot

	tikzcoordinate( SRC, SRC )
	tikzcoordinate( SRC_n, SRC.n )
	tikzcoordinate( SRC_s, SRC.s )
	tikzcoordinate( PRIM1_n, PRIM1.n )
	tikzcoordinate( PRIM1_s, PRIM1.s )
	tikzcoordinate( PRIM2_n, PRIM2.n )
	tikzcoordinate( PRIM2_s, PRIM2.s )
	tikzcoordinate( SW1_n, (SW1.x,PRIM1.n.y) )
	tikzcoordinate( SW1_s, SW1.s )
	tikzcoordinate( SW2_s, SW2.s )
	tikzcoordinate( SEC1_n, SEC1.n )
	tikzcoordinate( SEC1_s, SEC1.s )
	tikzcoordinate( SEC2_n, SEC2.n )
	tikzcoordinate( SEC2_s, SEC2.s )
	tikzcoordinate( D1_n, D1.n )
	tikzcoordinate( D1_s, D1.s )
	tikzcoordinate( D2_n, D2.n )
	tikzcoordinate( D2_s, D2.s )
	tikzcoordinate( D3_n, D3.n )
	tikzcoordinate( D3_s, D3.s )
	tikzcoordinate( D4_n, D4.n )
	tikzcoordinate( D4_s, D4.s )
	tikzcoordinate( CAP1_n, CAP1.n )
	tikzcoordinate( CAP1_s, CAP1.s )
	tikzcoordinate( CAP2_n, CAP2.n )
	tikzcoordinate( CAP2_s, CAP2.s )
	tikzcoordinate( LOADP_n, LOADP.n )
	tikzcoordinate( LOADN_s, LOADN.s )
	tikzcoordinate( AMP, AMP.c )

#	tikzcoordinate( LOAD_n, LOAD.n )
#	tikzcoordinate( LOAD_s, LOAD.s )

.PE
