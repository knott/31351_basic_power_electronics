.PS
cct_init
log_init
# central block library:
include(/Users/aknott/Documents/research/01_drawings/blocks.m4)
##### allowing nodes to be refered by tikz later
### from personal e-mail correspondance with D. Aplevich in 2017:
dnl	`tikznode(name,location,string)'
define(`tikznode', `command sprintf("\draw (%g,%g) node [inner sep=0pt, minimum size=0pt] (`$1'){`$3'};", (`$2').x/scale, (`$2').y/scale)')
dnl	`tikzcoordinate(name,location)'
define(`tikzcoordinate', `command sprintf("\draw (%g,%g) node [shape=coordinate] (`$1'){};", (`$2').x/scale, (`$2').y/scale)')
##### add uncover for animating slides with beamer
## usage:
## setuncover(start,end)
## <whatever code should be animated>
## resetuncover
## where `start' is first slide to show and `end' is last slide (if all, use `-')
dnl   `setuncover(start,end)'
define(`setuncover', `command sprintf("\uncover<$1$2>{")')
dnl   `resetuncover()'
define(`resetuncover', `command sprintf("}")')
###### same for only command:
dnl   `setonly(start,end)'
define(`setonly', `command sprintf("\only<$1$2>{")')
dnl   `resetonly()'
define(`resetonly', `command sprintf("}")')
#
scale = 2.54		       # cm
elen = 2
#define(‘dimen_’,(dimen_*2))
#linethick = 2
linethick_(2.0)
arrowht = 0.25
#dimen_ = 3
### define DTU colors ###
# primary colors #
define( `dtured', `0.6, 0, 0' )
define( `dtugrey', `0.6 ,0.6, 0.6' )
# secondary colors #
define( `dtuyellow', `1.00, 0.80, 0.00' )
define( `dtuorange', `1.00, 0.60, 0.00' )
define( `dtulightred', `1.00, 0.00, 0.00' )
define( `dtubrown', `0.60, 0.00, 0.00' )
define( `dtupurple', `0.80, 0.20, 0.60' )
define( `dtuviolet', `0.40, 0.00, 0.60' )
define( `dtudarkblue', `0.20, 0.40, 0.80' )
define( `dtulightblue', `0.20, 0.80, 1.00' )
define( `dtulightgreen', `0.60, 0.80, 0.20' )
define( `dtudarkgreen', `0.40, 0.80, 0.00' )
define( `dtucoolgrey', `0.59, 0.58, 0.57' )
Origin: Here                   # Position names are capitalized

	line up_ elen_
SRC1:	source( up_ elen_, V, )#; llabel( , V_{in} )
	corner
	line right_ elen_
	dot
#	{
#		line down_ 0.2*elen_
#		b_current( I_{F1} )
#	}
FET1:	e_fet( up_ elen_ ) with .D at Here
	move to FET1.S
	dot
	{
		line right_ 0.5*elen_
		dot
		{
IND:			inductor( right_ elen_, L, )
			b_current( I_3, below_)
			corner
PRIM:			inductor( down_ elen_, L, ); llabel( N_1 )
			corner
			line to Origin
		}
BD1:		diode( up_ elen_ )
#		b_current( I_{BD1}, below_, O, E, )
		corner
		line to FET1.n
	}
	line down_ 1.5*elen_
#	{
#		line down_ 0.2*elen_
#		b_current( I_{F2} )
#	}
FET2:	e_fet( up_ elen_ ) with .D at Here
	move to FET2.S
	{
		dot
		line right_ 0.5*elen_
		corner
BD2:		diode( up_ elen_ )
#		b_current( I_{BD2}, below_, O, E, )
		corner
		line to FET2.D
		dot
	}
	line to ( SRC1.x, Here.y )
	corner
SRC2:	source( up_ 1.5*elen_, V, )
	dot

	move to PRIM.s + ( 0.9*elen_, 0 )
SEC:	inductor( up_ elen_, L, ); llabel( , , N_2 )
	corner
	line right_ 0.5*elen_
	dot
D1:	diode( up_ elen_ )
#	b_current( I_{D3}, , E, )
	corner
	line right_ 0.5*elen_
	dot
	line right_ 0.5*elen_
#	b_current( I_2 )
	corner
	line down_ elen_
SRCB:	reversed( `source', down_ elen_, V, )
	line down_ elen_
	corner
	line left_ 0.5*elen_
	dot
	{
D4:		diode( up_ elen_ )
		dot
		{
			line to SEC.s
			corner
		}
		line up_ elen_
D3:		diode( up_ elen_ )
#		b_current( I_{D4} )
	}
	line left_ 0.5*elen_
	corner
D2:	diode( up_ elen_ )
	line up_ elen_

# core and winding directions:
        move to PRIM.s + ( 0.4*elen_, 0.3*elen_ )
	line up_ 0.4*elen_
	move to PRIM.s + ( 0.5*elen_, 0.3*elen_ )
	line up_ 0.4*elen_
	move to PRIM.n + ( 0.1*elen_, -0.3*elen_ )
	dot
	move to SEC.n + ( -0.1*elen_, -0.3*elen_ )
	dot

	tikzcoordinate( SRC1_n, SRC1.n )
	tikzcoordinate( SRC1_s, SRC1.s )
	tikzcoordinate( SRC2_n, SRC2.n )
	tikzcoordinate( SRC2_s, SRC2.s )
	tikzcoordinate( FET1_G, FET1.G )
	tikzcoordinate( FET1_D, FET1.D )
	tikzcoordinate( FET1_S, FET1.S )
	tikzcoordinate( FET2_G, FET2.G )
	tikzcoordinate( FET2_D, FET2.D )
	tikzcoordinate( FET2_S, FET2.S )
	tikzcoordinate( IND_w, IND.w )
	tikzcoordinate( IND_e, IND.e )
	tikzcoordinate( SRCB_n, SRCB.n )
	tikzcoordinate( SRCB_s, SRCB.s )

.PE
