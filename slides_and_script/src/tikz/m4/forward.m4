.PS
cct_init
log_init
##### allowing nodes to be refered by tikz later
### from personal e-mail correspondance with D. Aplevich in 2017:
dnl	`tikznode(name,location,string)'
define(`tikznode', `command sprintf("\draw (%g,%g) node [inner sep=0pt, minimum size=0pt] (`$1'){`$3'};", (`$2').x/scale, (`$2').y/scale)')
dnl	`tikzcoordinate(name,location)'
define(`tikzcoordinate', `command sprintf("\draw (%g,%g) node [shape=coordinate] (`$1'){};", (`$2').x/scale, (`$2').y/scale)')
##### add uncover for animating slides with beamer
## usage:
## setuncover(start,end)
## <whatever code should be animated>
## resetuncover
## where `start' is first slide to show and `end' is last slide (if all, use `-')
dnl   `setuncover(start,end)'
define(`setuncover', `command sprintf("\uncover<$1$2>{")')
dnl   `resetuncover()'
define(`resetuncover', `command sprintf("}")')
###### same for only command:
dnl   `setonly(start,end)'
define(`setonly', `command sprintf("\only<$1$2>{")')
dnl   `resetonly()'
define(`resetonly', `command sprintf("}")')
#
scale = 2.54		       # cm
elen = 2
#define(‘dimen_’,(dimen_*2))
#linethick = 2
linethick_(2.0)
arrowht = 0.25
#dimen_ = 3
### define DTU colors ###
# primary colors #
define( `dtured', `0.6, 0, 0' )
define( `dtugrey', `0.6 ,0.6, 0.6' )
# secondary colors #
define( `dtuyellow', `1.00, 0.80, 0.00' )
define( `dtuorange', `1.00, 0.60, 0.00' )
define( `dtulightred', `1.00, 0.00, 0.00' )
define( `dtubrown', `0.60, 0.00, 0.00' )
define( `dtupurple', `0.80, 0.20, 0.60' )
define( `dtuviolet', `0.40, 0.00, 0.60' )
define( `dtudarkblue', `0.20, 0.40, 0.80' )
define( `dtulightblue', `0.20, 0.80, 1.00' )
define( `dtulightgreen', `0.60, 0.80, 0.20' )
define( `dtudarkgreen', `0.40, 0.80, 0.00' )
define( `dtucoolgrey', `0.59, 0.58, 0.57' )
Origin: Here                   # Position names are capitalized

SRC:	source( up_ 2*elen_, V, )#; llabel( , V_{in} )
	corner
	line right_ elen_
	dot
	{
DEMAG:		inductor( down_ elen_, L, ); llabel( N )
DPRIM:		reversed( `diode', down_ elen_ )
		dot
	}
	line right_ 0.9*elen_
	corner
PRIM:	inductor( down_ elen_, L, ); llabel( N_1 )
	{
		setonly( 1 ,)
		reversed( `switch', down_ elen_ )
		resetonly
	}
	{
		setonly( 2 ,)
		reversed( `lswitch', down_ elen_, , C )
		resetonly
	}
	setonly( 3 , - )
	reversed( `lswitch', down_ elen_, , O )
	resetonly
	corner
SW:	line to SRC.s
	corner

	move to PRIM.s + ( 0.9*elen_, 0 )
SEC:	inductor( up_ elen_, L, ); llabel( , , N_2 )
	corner
D1:	diode( right_ elen_ )
	dot
	{
D2:		reversed( `diode', down_ elen_ )
		dot
	}
LOUT:	inductor( right_ elen_, L, )
	dot
	{
CAP:		capacitor( down_ elen_ )
		dot
	}
	line right_ elen_
	corner
LOAD:	resistor( down_ elen_, , E, )
	corner
	line to SEC.s

# core and winding directions:
        move to DEMAG.s + ( 0.4*elen_, 0.3*elen_ )
	line up_ 0.4*elen_
	move to DEMAG.s + ( 0.5*elen_, 0.3*elen_ )
	line up_ 0.4*elen_
        move to PRIM.s + ( 0.4*elen_, 0.3*elen_ )
	line up_ 0.4*elen_
	move to PRIM.s + ( 0.5*elen_, 0.3*elen_ )
	line up_ 0.4*elen_
	move to DEMAG.s + ( 0.1*elen_, 0.3*elen_ )
	dot
	move to PRIM.n + ( 0.1*elen_, -0.3*elen_ )
	dot
	move to SEC.n + ( -0.1*elen_, -0.3*elen_ )
	dot

	tikzcoordinate( SRC, SRC )
	tikzcoordinate( SRC_n, SRC.n )
	tikzcoordinate( SRC_s, SRC.s )
	tikzcoordinate( DEMAG_n, DEMAG.n )
	tikzcoordinate( DEMAG_s, DEMAG.s )
	tikzcoordinate( DPRIM_s, DPRIM.s )
	tikzcoordinate( PRIM_n, PRIM.n )
	tikzcoordinate( PRIM_s, PRIM.s )
	tikzcoordinate( SW_s, SW.e )
	tikzcoordinate( SEC_n, SEC.n )
	tikzcoordinate( SEC_s, SEC.s )
	tikzcoordinate( D2_n, D2.n )
	tikzcoordinate( D2_s, D2.s )
	tikzcoordinate( CAP_n, CAP.n )
	tikzcoordinate( CAP_s, CAP.s )
	tikzcoordinate( LOAD_n, LOAD.n )
	tikzcoordinate( LOAD_s, LOAD.s )

.PE
