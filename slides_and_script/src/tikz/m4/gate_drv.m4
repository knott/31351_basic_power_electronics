.PS
cct_init
log_init
##### allowing nodes to be refered by tikz later
### from personal e-mail correspondance with D. Aplevich in 2017:
dnl	`tikznode(name,location,string)'
define(`tikznode', `command sprintf("\draw (%g,%g) node [inner sep=0pt, minimum size=0pt] (`$1'){`$3'};", (`$2').x/scale, (`$2').y/scale)')
scale = 2.54		       # cm
elen = 2
#define(‘dimen_’,(dimen_*2))
#linethick = 2
linethick_(2.0)
#dimen_ = 1.5
arrowht = 0.25
Origin: Here                   # Position names are capitalized


RES:	resistor( up_ elen_, ,E )
GATE:	move to RES.c + ( -0.5*elen_, -0.1*elen_ )
	line right_ 0.3*elen_
	arrow to Here + ( 0.5*elen_, 0.3*elen_ )

	move to GATE.w
	dot
CISS:	capacitor( down_ elen_ ); rlabel( , C_{iss} )
	dot
	{
		line to (RES.x,Here.y)
		corner
		line to RES.s
	}
	line left_ elen_
	dot
	{
LS:		switch( up_ elen_ )
		dot
		{
			line to CISS.n
		}
HS:		switch( up_ elen_ )
	}
	line left_ 0.5*elen_
	corner
	source( up_ 2*elen_, V, ); llabel( -, V_g, + )
	corner
	line to HS.n
	corner
	
	
#addtaps(<-,0.2,0.1*elen_)

.PE
