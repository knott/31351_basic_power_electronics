.PS
cct_init
log_init
# central block library:
include(/Users/aknott/Documents/research/01_drawings/blocks.m4)
##### allowing nodes to be refered by tikz later
### from personal e-mail correspondance with D. Aplevich in 2017:
dnl	`tikznode(name,location,string)'
define(`tikznode', `command sprintf("\draw (%g,%g) node [inner sep=0pt, minimum size=0pt] (`$1'){`$3'};", (`$2').x/scale, (`$2').y/scale)')
dnl	`tikzcoordinate(name,location)'
define(`tikzcoordinate', `command sprintf("\draw (%g,%g) node [shape=coordinate] (`$1'){};", (`$2').x/scale, (`$2').y/scale)')
##### add uncover for animating slides with beamer
## usage:
## setuncover(start,end)
## <whatever code should be animated>
## resetuncover
## where `start' is first slide to show and `end' is last slide (if all, use `-')
dnl   `setuncover(start,end)'
define(`setuncover', `command sprintf("\uncover<$1$2>{")')
dnl   `resetuncover()'
define(`resetuncover', `command sprintf("}")')
###### same for only command:
dnl   `setonly(start,end)'
define(`setonly', `command sprintf("\only<$1$2>{")')
dnl   `resetonly()'
define(`resetonly', `command sprintf("}")')
#
scale = 2.54		       # cm
elen = 2
#define(‘dimen_’,(dimen_*2))
#linethick = 2
linethick_(2.0)
arrowht = 0.25
#dimen_ = 3
### define DTU colors ###
# primary colors #
define( `dtured', `0.6, 0, 0' )
define( `dtugrey', `0.6 ,0.6, 0.6' )
# secondary colors #
define( `dtuyellow', `1.00, 0.80, 0.00' )
define( `dtuorange', `1.00, 0.60, 0.00' )
define( `dtulightred', `1.00, 0.00, 0.00' )
define( `dtubrown', `0.60, 0.00, 0.00' )
define( `dtupurple', `0.80, 0.20, 0.60' )
define( `dtuviolet', `0.40, 0.00, 0.60' )
define( `dtudarkblue', `0.20, 0.40, 0.80' )
define( `dtulightblue', `0.20, 0.80, 1.00' )
define( `dtulightgreen', `0.60, 0.80, 0.20' )
define( `dtudarkgreen', `0.40, 0.80, 0.00' )
define( `dtucoolgrey', `0.59, 0.58, 0.57' )
Origin: Here                   # Position names are capitalized

	
	line right_ 0.5*elen_; "$V_{desired}$" above
	arrow right_ 0.5*elen_
SUM:	circle elen_
	{
		move to SUM.w + ( 0.2*elen_, 0 )
		"$+$"
		move to SUM.s + ( 0, 0.15*elen_ )
		"$-$"
	}
	line right_ 0.5*elen_; "$error$" above
	arrow right_ 0.5*elen_
	box wid 1.3*elen_ "controller"
	line right_ 0.5*elen_; "$d$" above
	arrow right_ 0.5*elen_
PS:	box "power" "stage"
	{
		move to PS.n + ( 0, elen_ )
		"$V_{in}$" above
		arrow to PS.n
	}
	line right_ 0.5*elen_
	dot
	{
		arrow right_ 0.5*elen_
		"$V_{out}$" above
	}
	line down_ 1.5*elen_
	corner
	line left_ elen_
	box "sense"
	line left_ 1.5*elen_; "$k \cdot V_{out}$" above
	line to ( SUM.s.x, Here.y )
	corner
	arrow to SUM.s

.PE
