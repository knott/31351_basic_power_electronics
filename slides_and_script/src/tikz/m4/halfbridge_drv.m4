.PS
cct_init
log_init
# central block library:
include(/Users/aknott/Documents/research/01_drawings/blocks.m4)
##### allowing nodes to be refered by tikz later
### from personal e-mail correspondance with D. Aplevich in 2017:
dnl	`tikznode(name,location,string)'
define(`tikznode', `command sprintf("\draw (%g,%g) node [inner sep=0pt, minimum size=0pt] (`$1'){`$3'};", (`$2').x/scale, (`$2').y/scale)')
dnl	`tikzcoordinate(name,location)'
define(`tikzcoordinate', `command sprintf("\draw (%g,%g) node [shape=coordinate] (`$1'){};", (`$2').x/scale, (`$2').y/scale)')
##### add uncover for animating slides with beamer
## usage:
## setuncover(start,end)
## <whatever code should be animated>
## resetuncover
## where `start' is first slide to show and `end' is last slide (if all, use `-')
dnl   `setuncover(start,end)'
define(`setuncover', `command sprintf("\uncover<$1$2>{")')
dnl   `resetuncover()'
define(`resetuncover', `command sprintf("}")')
###### same for only command:
dnl   `setonly(start,end)'
define(`setonly', `command sprintf("\only<$1$2>{")')
dnl   `resetonly()'
define(`resetonly', `command sprintf("}")')
#
scale = 2.54		       # cm
elen = 2
#define(‘dimen_’,(dimen_*2))
#linethick = 2
linethick_(2.0)
arrowht = 0.25
#dimen_ = 3
### define DTU colors ###
# primary colors #
define( `dtured', `0.6, 0, 0' )
define( `dtugrey', `0.6 ,0.6, 0.6' )
# secondary colors #
define( `dtuyellow', `1.00, 0.80, 0.00' )
define( `dtuorange', `1.00, 0.60, 0.00' )
define( `dtulightred', `1.00, 0.00, 0.00' )
define( `dtubrown', `0.60, 0.00, 0.00' )
define( `dtupurple', `0.80, 0.20, 0.60' )
define( `dtuviolet', `0.40, 0.00, 0.60' )
define( `dtudarkblue', `0.20, 0.40, 0.80' )
define( `dtulightblue', `0.20, 0.80, 1.00' )
define( `dtulightgreen', `0.60, 0.80, 0.20' )
define( `dtudarkgreen', `0.40, 0.80, 0.00' )
define( `dtucoolgrey', `0.59, 0.58, 0.57' )
Origin: Here                   # Position names are capitalized
 
#PULSE:  line right_ 0.5*elen_
#	dot
#	{
#		line up_ elen_
#		corner
#		arrow right_ 0.5*elen_
#HSLS:		box ht 1.5*elen_ "high" "side" "level" "shifter"
#		move to HSLS.e
#		arrow right_ 1.5*elen_
#HSGD:		box ht 1.5*elen_ "high" "side" "gate" "driver"
#		move to HSGD.e
#		line right_ 0.3*elen_
#HS:		e_fet( up_ elen_ ) with .G at Here
#	}
#	line down_ elen_
#	corner
#	arrow right_ 0.5*elen_
#LSLS:	box ht 1.5*elen_ "low" "side" "level" "shifter"
#	move to LSLS.e
#	arrow right_ 1.5*elen_
#LSGD:	box ht 1.5*elen_ "low" "side" "gate" "driver"
#	move to LSGD.e
#	line right_ 0.3*elen_
#LS:	e_fet( up_ elen_ ) with .G at Here 
#
# connecting the power stage:
#
# adding the bootstrap:
#

IN:	"$p$" above
	line right 0.5 * elen_
INDOT:	dot
	{
		line down 1 * elen_
		corner
		arrow right 0.6 * elen_
LSLS:		box wid elen_ ht 1.2 * elen_ "low" "side" "level" "shifter"
		{
			line up 0.2 * elen_ from LSLS.n
VGD:			dot
			line up 0.2 * elen_
			dot( , , 1 )
			"$V_{gate drive}$" above
		}
PWMLOW:		line right 1.2 * elen_
		"$p_{L}$" above
		arrow right 0.4 * elen_
LSGD:		box wid elen_ ht 1.2 * elen_ "low" "side" "gate" "driver"
		{
LSGDSPLY:		line up 0.2 * elen_ from LSGD.n
			corner
			line left 1.7 * elen_
CGDPOS:			dot
			{
				#line down 0.8 * elen_
				crossover( down 0.9 * elen_ , , PWMLOW ) ######################### her har jeg sluttet #######################
CGD:				capacitor( down to ( Here.x, LSGD.s.y - 0.2 * elen_ ) )
				#capacitor( down to ( Here.x, LSGD.s.y - 0.2 * elen_ ) ); rlabel( , , C_{gd} )
CGDNEG:				dot
				{
					line to ( LSLS.x, Here.y )
					corner
					line to LSLS.s
				}
				line to ( LSGD.x, Here.y )
LSGDNEGSPLY:			dot
				line to LSGD.s
			}
			line to VGD
		}
		line right 0.4 * elen_
LSFET:		e_fet( up elen_ ) with .G at Here
		move to LSFET.S
		line to ( Here.x, CGDNEG.y )
VNEG:		dot
		{
			line to LSGDNEGSPLY
		}
		line down 0.2 * elen_
		dot( , , 1 )
		"$-V_s$" below
	}
	line up 1 * elen_
	corner
	arrow right 0.6 * elen_
HSLS:	box wid elen_ ht 1.2 * elen_ "high" "side" "level" "shifter"


PWMHI:	line right 1.2 * elen_
	"$p_H$" above
	{
		move to INDOT
		{
			line up 0.2 * elen_ from HSLS.n
			corner
BSDIOCAT:		line to ( CGD.x, Here.y )
			move to CGDPOS
BSDIODE:		diode( up_ elen_ )
			# line 
			crossover( to BSDIOCAT.e, , PWMHI )
			# diode( up from CGDPOS to Here, S, R )
BSDIODEPOS:		dot
			line right 0.4 * elen_
BSCAPPOS:		dot
		}
	}
	arrow right 0.4 * elen_
HSGD:	box wid elen_ ht 1.2 * elen_ "high" "side" "gate" "driver"
	line right 0.4 * elen_
HSFET:	e_fet( up elen_ ) with .G at Here
	move to HSFET.D
	line up 0.4 * elen_
VPOS:	dot( , , 1 )
	"$+V_s$" above
	{
		line down 0.2 * elen_ from HSGD.s
HSGDNEGSPLY:	dot
		{
BSCAPNEG:			line to ( BSCAPPOS.x, Here.y )
				corner
BSCAP:				#capacitor( up to BSCAPPOS )#; rlabel( , , C_{bs} )
				capacitor( up 0.7 * elen_ )
				crossover( to BSCAPPOS, , PWMHI)
		}
		{
HSGDSPLY:		line from HSGD.n to ( Here.x, BSCAPPOS.y )
			corner
			line to BSCAPPOS
		}
	}
	
	move to HSFET.S
	line to ( Here.x, BSCAPNEG.y )
SN:	dot
	{
		line to HSGDNEGSPLY
	}
	{
OUT:		arrow right 0.5 * elen_
		"$SN$" above
	}
	line to LSFET.D

	tikzcoordinate( IN, IN.w )
	tikzcoordinate( BSDIODEPOS, BSDIODEPOS )
	tikzcoordinate( CGDPOS, CGDPOS )
	tikzcoordinate( CGDNEG, CGDNEG )
	tikzcoordinate( BSCAPPOS, BSCAPPOS )
	tikzcoordinate( BSCAPNEG, BSCAPNEG.w )
	tikzcoordinate( HSGDSPLY, HSGDSPLY.n )
	tikzcoordinate( LSGDSPLY, LSGDSPLY.n )
	tikzcoordinate( LSGD, LSGD )
	tikzcoordinate( SN, SN )
	tikzcoordinate( LSFET, (LSFET.S.x,LSFET.G.y) )
	tikzcoordinate( HSFET, (HSFET.S.x,HSFET.G.y) )
	tikzcoordinate( HSGD, HSGD )
	tikzcoordinate( VNEG, VNEG )


.PE
