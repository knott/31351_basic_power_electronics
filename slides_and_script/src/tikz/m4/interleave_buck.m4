.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here                   # Position names are capitalized

SRC:  source( up_ 3*elen_, V, )
      corner
      line right_ 0.7*elen_
      dot
      {
	line right_ 0.7*elen_
	corner
SWB2:	reversed( `switch', down_ elen_ )
	dot
	{
L1:	  inductor( right_ elen_, W, )#; llabel ( , L_{1},  )
	  setuncover(2,-)
	  setrgb( dtudarkgreen )
	  b_current( I_{L1} )
	  resetrgb
	  resetuncover
	  corner
	  line down_ elen_
	  dot
	  {
CAP:		capacitor( down_ elen_ ); rlabel( , C )
		setuncover(3,-)
		setrgb( dtured )
		b_current( I_{C})
		resetrgb
		resetuncover
		dot
	  }
      	  line right_ elen_
	  setuncover(3,-)
	  setrgb( dtuorange )
	  b_current( I_{out} )
	  resetrgb
	  resetuncover
      	  corner
LOAD: 	  resistor( down_ elen_, E ); llabel( , R_L )
      	  corner
	  line to SRC.s
	  corner
	}
	line down_ elen_
SWA2:	reversed( `switch', down_ elen_ )
	dot
      }
SWA1: reversed( `switch', down_ elen_ )
      line down_ elen_
      dot
      {
	line right_ 0.7*elen_
L2:	inductor( right_ elen_, W, )#; llabel ( , L_{2}, )
	setuncover(2,-)
	setrgb( dtudarkblue )
	b_current( I_{L2} )
	resetrgb
	resetuncover
      }
SWB1: reversed( `switch', down_ elen_ )
      dot
      

      tikzcoordinate( SWA1, SWA1 )
      tikzcoordinate( SWA2, SWA2 )
      tikzcoordinate( SWB1, SWB1 )
      tikzcoordinate( SWB2, SWB2 )

.PE
