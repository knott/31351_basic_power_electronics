.PS
cct_init
log_init
# central block library:
include(/Users/aknott/Documents/research/01_drawings/blocks.m4)
##### allowing nodes to be refered by tikz later
### from personal e-mail correspondance with D. Aplevich in 2017:
dnl	`tikznode(name,location,string)'
define(`tikznode', `command sprintf("\draw (%g,%g) node [inner sep=0pt, minimum size=0pt] (`$1'){`$3'};", (`$2').x/scale, (`$2').y/scale)')
dnl	`tikzcoordinate(name,location)'
define(`tikzcoordinate', `command sprintf("\draw (%g,%g) node [shape=coordinate] (`$1'){};", (`$2').x/scale, (`$2').y/scale)')
##### add uncover for animating slides with beamer
## usage:
## setuncover(start,end)
## <whatever code should be animated>
## resetuncover
## where `start' is first slide to show and `end' is last slide (if all, use `-')
dnl   `setuncover(start,end)'
define(`setuncover', `command sprintf("\uncover<$1$2>{")')
dnl   `resetuncover()'
define(`resetuncover', `command sprintf("}")')
###### same for only command:
dnl   `setonly(start,end)'
define(`setonly', `command sprintf("\only<$1$2>{")')
dnl   `resetonly()'
define(`resetonly', `command sprintf("}")')
#
scale = 2.54		       # cm
elen = 2
#define(‘dimen_’,(dimen_*2))
#linethick = 2
linethick_(2.0)
arrowht = 0.25
#dimen_ = 3
### define DTU colors ###
# primary colors #
define( `dtured', `0.6, 0, 0' )
define( `dtugrey', `0.6 ,0.6, 0.6' )
# secondary colors #
define( `dtuyellow', `1.00, 0.80, 0.00' )
define( `dtuorange', `1.00, 0.60, 0.00' )
define( `dtulightred', `1.00, 0.00, 0.00' )
define( `dtubrown', `0.60, 0.00, 0.00' )
define( `dtupurple', `0.80, 0.20, 0.60' )
define( `dtuviolet', `0.40, 0.00, 0.60' )
define( `dtudarkblue', `0.20, 0.40, 0.80' )
define( `dtulightblue', `0.20, 0.80, 1.00' )
define( `dtulightgreen', `0.60, 0.80, 0.20' )
define( `dtudarkgreen', `0.40, 0.80, 0.00' )
define( `dtucoolgrey', `0.59, 0.58, 0.57' )
Origin: Here                   # Position names are capitalized

OPAMP:	opamp( elen_ )
	move to OPAMP.E1
	line up_ 0.4*elen_; "$V_{dd}$" above
	dot( , , 1 )
	move to OPAMP.E2
	line down_ 0.4*elen_; "$V_{ss}$" below
	dot( , , 1 )
	
	move to OPAMP.In1
INN:	line left_ elen
	move to OPAMP.In2
INP:	line left_ 0.5*elen
	move to OPAMP.Out
	corner
	line up_ elen_
	corner
	line to (INP.w.x,Here.y)
	dot
	{
RH1:		resistor( down_ elen_, E, )
		line to INP.w
		dot
RH2:		resistor( down_ elen_, E, )
GND:		ground
	}
	line to (INN.w.x,Here.y)
	corner
RES:	resistor( down_ elen_, E, )
	line to INN.w
	dot
	capacitor( to (Here.x,GND.y) )
	ground

	setuncover(10,)
	move to INN.w
RIN:	resistor( left_ elen_, E, )
	corner
	source( to (Here.x,GND.y), AC, )
	ground
	resetuncover

	tikzcoordinate( OUT, OPAMP.Out )
	tikzcoordinate( INP, INP.w )
	tikzcoordinate( INN, INN.w )

.PE
