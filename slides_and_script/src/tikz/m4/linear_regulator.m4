.PS
# Test.m4
cct_init
log_init
# Use this file to do a quick test of diagrams you are developing.
# Enter the drawing commands here and (if you have a "make" utility) type
#   make tst
# Otherwise, to process a file called filename.m4, type one of
#   make filename.ps
#   make filename.eps
#   make filename.pdf
#   make filename.gif (requires ImageMagick convert)
#   make filename.png (requires ImageMagick convert)
#   make filename.tif (requires ImageMagick convert)
# To perform tests in a new folder, copy the Makefile, this file,
#   and tst.tex (or your own equivalent) to that folder.
# cct_init                       # Read in macro definitions and set defaults
# elen = 0.75                    # Variables are allowed; default units are inches
scale = 2.54		       # cm
elen = 2
#linethick = 2
linethick_(2.0)
Origin: Here                   # Position names are capitalized
#
# AC mains, rectifier bridge, half-wave rectifier and input decoupuling capacitor:
VIN: source(up_ 2*elen_, V); llabel(,V_{in})
     corner
     line right_ elen_
     dot
     {
CIN:	capacitor(to (Here.x,VIN.s.y))
	rlabel(C_{in})
	dot
     }
     line right_ elen_
     dot
     {
RES:	resistor(down_ elen_,E); rlabel(R)
	dot
ZENER:	diode(up_ elen_ with .n at Here,Z)
	move to ZENER.s
	dot
     }
     line right_ 0.5*elen_
TRANS: bi_tr(left_ elen_) with .C at Here
     move to TRANS.E
     line right_ 0.5*elen_
     dot
     {
COUT:	capacitor(to (Here.x,VIN.s.y))
	rlabel(C_{out})
	dot
     }
     line right_ elen_
     corner
LOAD: resistor(down_ 2*elen_,E); rlabel(,,R_{load})
     corner
     line to VIN.s
     corner

     move to RES.s
     line to (TRANS.B.x,Here.y)
     corner
     line to TRANS.B


.PE
