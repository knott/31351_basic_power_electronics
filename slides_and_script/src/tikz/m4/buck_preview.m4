.PS
cct_init
log_init
##### allowing nodes to be refered by tikz later
### from personal e-mail correspondance with D. Aplevich in 2017:
dnl	`tikznode(name,location,string)'
define(`tikznode', `command sprintf("\draw (%g,%g) node [inner sep=0pt, minimum size=0pt] (`$1'){`$3'};", (`$2').x/scale, (`$2').y/scale)')
scale = 2.54		       # cm
elen = 2
#define(‘dimen_’,(dimen_*2))
#linethick = 2
linethick_(2.0)
#dimen_ = 3
Origin: Here                   # Position names are capitalized

#line right_ 0.4*elen_

SRC:  source( up_ 2*elen_, V, )
      corner
      line right_ elen_
#      corner

      move to SRC.s
      corner
      line right_ elen_
      corner
BUCK:  diode( up_ elen_ )
SN:   dot
      switch( up_ elen_ )
      corner

      move to SN
      inductor( right_ 2*elen_, L, )
      dot
      {
	capacitor( down_ elen_ )
	dot
      }
      line right_ elen_
      corner
      resistor( down_ elen_, E )
      corner
      line to BUCK.s
      dot

.PE
