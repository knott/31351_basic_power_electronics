.PS
cct_init
log_init
##### allowing nodes to be refered by tikz later
### from personal e-mail correspondance with D. Aplevich in 2017:
dnl	`tikznode(name,location,string)'
define(`tikznode', `command sprintf("\draw (%g,%g) node [inner sep=0pt, minimum size=0pt] (`$1'){`$3'};", (`$2').x/scale, (`$2').y/scale)')
dnl	`tikzcoordinate(name,location)'
define(`tikzcoordinate', `command sprintf("\draw (%g,%g) node [shape=coordinate] (`$1'){};", (`$2').x/scale, (`$2').y/scale)')
scale = 2.54		       # cm
elen = 2
#define(‘dimen_’,(dimen_*2))
#linethick = 2
linethick_(2.0)
arrowht = 0.25
#dimen_ = 3
### define DTU colors ###
# primary colors #
define( `dtured', `0.6, 0, 0' )
define( `dtugrey', `0.6 ,0.6, 0.6' )
# secondary colors #
define( `dtuyellow', `1.00, 0.80, 0.00' )
define( `dtuorange', `1.00, 0.60, 0.00' )
define( `dtulightred', `1.00, 0.00, 0.00' )
define( `dtubrown', `0.60, 0.00, 0.00' )
define( `dtupurple', `0.80, 0.20, 0.60' )
define( `dtuviolet', `0.40, 0.00, 0.60' )
define( `dtudarkblue', `0.20, 0.40, 0.80' )
define( `dtulightblue', `0.20, 0.80, 1.00' )
define( `dtulightgreen', `0.60, 0.80, 0.20' )
define( `dtudarkgreen', `0.40, 0.80, 0.00' )
define( `dtucoolgrey', `0.59, 0.58, 0.57' )
Origin: Here                   # Position names are capitalized

	ground( , , P, )
	line up_ 0.25*elen_
GND:	dot
	{
		line right_ 0.5*elen_
		line right_ elen_ dashed
		line right_ 0.5*elen_; "$PE$" above
		line right_ 0.75*elen_
		corner
PE:		line up_ elen_
		dot
	}
	line up_ 2*elen_
N:	dot
	{
		line right_ 0.5*elen_
		line right_ elen_ dashed
		line right_ 0.5*elen_; "$N$" above
		line right_ elen_
		dot
		{
			line up_ 0.75*elen_
			diode( up_ 0.75*elen_ )
			dot
		}
		reversed( `diode', down_ 0.75* elen_ )
		dot
	}
SRC:	source( up_ 0.75*elen_, AC, )
	corner
	line right_ 0.5*elen_
	line right_ elen_ dashed
	line right_ 0.5*elen_; "$L_1$" above
	line right_ 0.5*elen_
	dot
D1:	diode( up_ 0.75*elen_ )
	corner
	line right_ elen_
	dot
	{
		capacitor( down_ 2.25*elen_ )
		dot
	}
	line right_ 0.5*elen_
	corner
SW:	reversed( `switch', down_ 1.125*elen_ )
SN:	dot
	{
		reversed( `diode', down_ 1.125*elen_ )
		dot
	}
IND:	inductor( right_ 0.75*elen_, W, )
	dot
	{
CAP:		capacitor( down_ 1.125*elen_ )
		dot
	}
	line right_ 0.75* elen_
	corner
MOTOR:	ttmotor( down_ 1.125*elen_ )
	corner
	line to (D1.x,Here.y)
	corner
D2:	diode( up_ 0.75*elen_ )
	line up_ 0.75*elen_

	tikzcoordinate( GND, GND )
	tikzcoordinate( SRC_n, SRC.n )
	tikzcoordinate( SRC_s, SRC.s )
	tikzcoordinate( PE, PE.s )
	tikzcoordinate( D1_s, D1.s )
	tikzcoordinate( D1_n, D1.n )
	tikzcoordinate( SW_n, SW.n )
	tikzcoordinate( SN, SN )
	tikzcoordinate( CAP_s, CAP.s )
	tikzcoordinate( MOTOR_n, MOTOR.n )
	tikzcoordinate( MOTOR, MOTOR )
	tikzcoordinate( D2_s, D2.s )

.PE
