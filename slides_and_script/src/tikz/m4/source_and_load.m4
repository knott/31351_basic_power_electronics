.PS
# Test.m4
cct_init
log_init
# Use this file to do a quick test of diagrams you are developing.
# Enter the drawing commands here and (if you have a "make" utility) type
#   make tst
# Otherwise, to process a file called filename.m4, type one of
#   make filename.ps
#   make filename.eps
#   make filename.pdf
#   make filename.gif (requires ImageMagick convert)
#   make filename.png (requires ImageMagick convert)
#   make filename.tif (requires ImageMagick convert)
# To perform tests in a new folder, copy the Makefile, this file,
#   and tst.tex (or your own equivalent) to that folder.
# cct_init                       # Read in macro definitions and set defaults
# elen = 0.75                    # Variables are allowed; default units are inches
##### allowing nodes to be refered by tikz later
### from personal e-mail correspondance with D. Aplevich in 2017:
# dnl                      ` namedstr(name,string,location)'
# define(`namedstr',`command sprintf("\draw (%g,%g) node(`$1'){`$2'};",(`$3').x,(`$3').y)')
dnl	`tikznode(name,location,string)'
define(`tikznode', `command sprintf("\draw (%g,%g) node [inner sep=0pt, minimum size=0pt] (`$1'){`$3'};", (`$2').x/scale, (`$2').y/scale)')
scale = 2.54		       # cm
elen = 2
#linethick = 2
linethick_(2.0)
Origin: Here                   # Position names are capitalized

PWRSRC:	source(up_ elen_, AC)#; rlabel(-,v(t),+)
	corner
VOLT:	line right_ elen_
	line right_ elen_
	b_current(i( t ), , O, E, 0.2)
	line right_ elen_
	{
		dot
LOADR:		resistor(down_ elen_, E)
		dot
	}
	line right_ 0.5*elen_
	{
		dot
LOADC:		capacitor(down_ elen_ )
		dot
	}
	line right_ 0.5*elen_
	corner
LOADL:	inductor(down_ elen_, W)
	corner
	line to PWRSRC.s
	corner

	move to VOLT.end + (0, -0.1*elen_ )
VARR:	arrow down_ 0.8*elen_
	"$v( t )$" at VARR + (0.3*elen_, 0)
	
	tikznode(PWRSRC_s,PWRSRC.s)
	tikznode(PWRSRC_n,PWRSRC.n)
	tikznode(LOADR_n,LOADR.n)
	tikznode(LOADL_s,LOADL.s)
	tikznode(LOADC_s,LOADC.s)
.PE
