.PS# 2.5
#include( HOMELIB_`'liblog.m4 )
log_init
cct_init
sfg_init
#sinclude( block_main.dim )
s_init( unique label )
#scale = 2*scale/2.54
scale = 2.54
linethick_(2.0)
# central block library:
#include( c:\research\own_research\03_drawings\blocks.m4 )

capacitor( right_ elen_ ); llabel( , C )
resistor( right_ elen_, E, ); llabel( , ESR )
inductor( right_ elen_, L, ); llabel( , ESL )

.PE
