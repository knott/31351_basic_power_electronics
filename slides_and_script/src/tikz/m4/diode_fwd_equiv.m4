.PS
cct_init
log_init
##### allowing nodes to be refered by tikz later
### from personal e-mail correspondance with D. Aplevich in 2017:
dnl	`tikznode(name,location,string)'
define(`tikznode', `command sprintf("\draw (%g,%g) node [inner sep=0pt, minimum size=0pt] (`$1'){`$3'};", (`$2').x/scale, (`$2').y/scale)')
scale = 2.54		       # cm
elen = 2
#define(‘dimen_’,(dimen_*2))
#linethick = 2
linethick_(2.0)
#dimen_ = 3
arrowht = 0.25
Origin: Here                   # Position names are capitalized

#line right_ 0.4*elen_

tikznode( LEFT, Here )
reversed( `source', right_ elen_, V, ); rlabel( +, V_{d_{pn}}, - )
resistor( right_ elen_, E ); rlabel( + , V_{d_{res}}, - )
tikznode( RIGHT, Here )

line right_ 0.4*elen_
b_current( I_{d_{fwd}} )


.PE
