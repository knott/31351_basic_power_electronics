.PS
cct_init
log_init
##### allowing nodes to be refered by tikz later
### from personal e-mail correspondance with D. Aplevich in 2017:
dnl	`tikznode(name,location,string)'
define(`tikznode', `command sprintf("\draw (%g,%g) node [inner sep=0pt, minimum size=0pt] (`$1'){`$3'};", (`$2').x/scale, (`$2').y/scale)')
##### add uncover for animating slides with beamer
## usage:
## setuncover(start,end)
## <whatever code should be animated>
## resetuncover
##
## where `start' is first slide to show and `end' is last slide (if all, use `-')
dnl   `setuncover(start,end)'
define(`setuncover', `command sprintf("\uncover<$1$2>{")')
dnl   `resetuncover()'
define(`resetuncover', `command sprintf("}")')
scale = 2.54		       # cm
#elen = 2
#define(‘dimen_’,(dimen_*2))
#linethick = 2
linethick_(2.0)
#dimen_ = 2
arrowht = 0.25
Origin: Here                   # Position names are capitalized

	down_
JUNC:	dot; llabel( ,\vartheta_j )
	
	setuncover(2,-)
	resistor( down_ 0.75*elen_, E,); llabel( , R_{th_{jc}})
CASE:	dot; llabel( ,\vartheta_c )
	resetuncover

	setuncover(3,-)
	resistor( down_ 0.75*elen_, E,); llabel( , R_{th_{cs}})
SINK:	dot; llabel( ,\vartheta_s )
	resetuncover

	setuncover(4,-)
	resistor( down_ 0.75*elen_, E,); llabel( , R_{th_{sa}})
AMB:	dot; llabel( ,\vartheta_a )
	move to Here + (0,-0.75*elen_)
GND:	dot; ground( at Here )
	resetuncover

	setuncover(5,-)
	line left_ 0.5*elen_
	corner
#	source( up_ 3*elen_, Q, )
	source( up_ 3*elen_, I, )
	b_current( P_d )
	corner
	line to JUNC
	resetuncover

	tikznode( AMB, AMB )
	tikznode( GND, GND )
	
.PE
