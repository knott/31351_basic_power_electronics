.PS
cct_init
log_init
##### allowing nodes to be refered by tikz later
### from personal e-mail correspondance with D. Aplevich in 2017:
dnl	`tikznode(name,location,string)'
define(`tikznode', `command sprintf("\draw (%g,%g) node [inner sep=0pt, minimum size=0pt] (`$1'){`$3'};", (`$2').x/scale, (`$2').y/scale)')
dnl	`tikzcoordinate(name,location)'
define(`tikzcoordinate', `command sprintf("\draw (%g,%g) node [shape=coordinate] (`$1'){};", (`$2').x/scale, (`$2').y/scale)')
scale = 2.54		       # cm
elen = 2
#define(‘dimen_’,(dimen_*2))
#linethick = 2
linethick_(2.0)
arrowht = 0.25
#dimen_ = 3
Origin: Here                   # Position names are capitalized

#line right_ 0.4*elen_

DIODE:  diode( up_ elen_ )

	move to DIODE.s + ( 2*elen_, 0 )
FET:	e_fet( up_ elen_ ) with .S at Here

      tikzcoordinate( DIODE, DIODE )
      tikzcoordinate( MOSFET, FET )


.PE
