# SHELL := /bin/bash
# feature requests:
# add an option where only graphics are complied (a) a specific one "graphics" b) all graphics "graphicsall")
# add an option "dist" for distribution, i.e. automatic upload e.g. to DTU learn
# add an option "archive" for compressing everything
# add an option "help"

#################################
##### ORGANIZATION OF FILE ######
#################################
#
# ORGANIZATION OF FILE
#
# DEFINITION OF VARIABLES
# -> LOCATION OF FILES
# -> COMMAND EXECUTION STUFF
# -> MULTI-LINE VARIABLE
# -> WHERE TO FIND THE SOURCES
#
# RULES
# -> PHONY RULES
# -> BUILD RULES
# -> CLEAN UP ALL THAT MESS
#
# within each section the order
# m4 -> pic -> tikz -> tex -> pdf
# is prefered
#################################

#################################
#### DEFINITION OF VARIABLES ####
#################################

QUIET = -quiet
#QUIET =

#### LOCATION OF FILES ####
PRESENTATION_FILE = presentation
SCRIPT_FILE = script

THIS_MAKEFILE_NAME = Makefile

SRC_DIR = ./src
SOURCES = $(shell find $(SRC_DIR) -type f)

BUILD_DIR = ./build

OUT_DIR = ./output

TIKZ_DIR = tikz

M4_DIR = m4

#BUILD_M4_DIR = ./build/tikz/m4
BUILD_M4_DIR = $(BUILD_DIR)/$(TIKZ_DIR)/$(M4_DIR)
M4DEP_FILES = $(wildcard $(BUILD_M4_DIR)/*.dep)
M4LIB_PATH = /usr/local/texlive/texmf-local/tex/latex/local/circuit_macros
M4INCLUDE_PATH = $(BUILD_DIR)/include/tikz/m4
M4INCLUDE_FILES = $(wildcard $(M4INCLUDE_PATH)/*.m4)
M4_PGF = pgf.m4
M4LIB_FILES = $(wildcard $(M4LIB_PATH)/*.m4)

#BUILD_TIKZ_DIR = ./build/tikz
BUILD_TIKZ_DIR = $(BUILD_DIR)/$(TIKZ_DIR)
TIKZ_FILEXT = .tikz.tex
TIKZDEP_FILES = $(wildcard $(BUILD_TIKZ_DIR)/*.dep)
TIKZSET_FILE = ./include/tikzset.tex

TEXDEP_FILES = $(wildcard $(BUILD_DIR)/*.dep)

#### COMMAND EXECUTION STUFF ####
# M4 specific stuff for circtuit macros:
# for more information consider
# https://ctan.org/pkg/circuit-macros?lang=en
M4 ?= m4 -I $(M4LIB_PATH) -I $(M4INCLUDE_PATH) $(M4_PGF)

# pic specific stuff for circuit macros
# for more information consider
# https://gitlab.com/aplevich/dpic
DPIC_G ?= dpic -g

# specify which latex complier to use:
#LATEX = pdflatex
LATEX = xelatex
#LATEX = latex # does not work. needs a conversion from dvi (to ps) to pdf - if not done by latexmk; viewer does not work either 

# call preview:
PREVIEW_OPT = pv
# for using "Shift+CMD+Click" to switch and synchronize view between skim and aquamacs
# PREVIEW_OPT = pvc

###### see if -g option in really necessary for both runs.
## Might only be required for the last run,
## therefore safe valueable processing time

# latexmk commands:
LATEXMK ?= latexmk
LATEXMK_OPT = -$(LATEX) -$(PREVIEW_OPT) -f -g -use-make -interaction=nonstopmode -file-line-error $(QUIET) -deps-out=$(BUILD_DIR)/$*.dep -outdir=$(BUILD_DIR)

# tikzmake commands:
TIKZ_PREFIX = $(TIKZ_DIR)/$(basename $(basename $(notdir $1)))_
TIKZ_SETNEXTFILENAME = $(TIKZ_DIR)/$(basename $(basename $(notdir $1)))
## debugging to get to work with ECHO:
TIKZ_CMD = $(LATEXMK) -$(LATEX) \\tikzexternalcheckshellescape -f -use-make -halt-on-error -interaction=nonstopmode -file-line-error -jobname="\image" $(QUIET) -usepretex="\def\\\\tikzexternalrealjob{\\tikzexternalrealjob}" "\\tikzexternalrealjob"

## works with SED:
# TIKZ_CMD = $(LATEXMK) -$(LATEX) \\tikzexternalcheckshellescape -f -use-make -halt-on-error -interaction=nonstopmode -jobname="\\image" $(QUIET) -usepretex="\\def\\\\tikzexternalrealjob\{\\tikzexternalrealjob\}" "\\tikzexternalrealjob"

### this one worked on the command line:
# latexmk -xelatex -use-make -halt-on-error -interaction=batchmode -jobname="tikz/buck" -usepretex="\def\tikzexternalrealjob{presentation}" "presentation"  

# editing files:
GREP ?= grep
SED ?= sed

# filesystems operations:
CD ?= cd
CP ?= cp
MKDIR_P ?= mkdir -p
PWD ?= pwd
RM ?= rm
RSYNC ?= rsync $(QUIET)

# storring current working directory:
# use CURDIR instead of following line!
#PWD := ${PWD}

# other shell stuff:
CAT ?= cat
ECHO ?= echo

# help text for this Makefile:
HELP = "\nHere is a list of the available commands:\n\
	\n\
	make\n\
	\t same as make all\n\
	\n\
	make all\n\
	\t creates both presentation and script\n\
	\n\
	make presentation\n\
	\t creates presentation only\n\
	\n\
	make script\n\
	\t creates script only\n\
	\n\
	make clean\n\
	\t deletes everything in the build directory\n\
	\n\
	make cleanall\n\
	\t deletes the build and the output directory\n\
	\n\
	make help\n\
	\t displays this help\n\
	\n\
	"

#### MULTI-LINE VARIABLES ####
# for code, thath is used often
# use $(call <variable>) to call

# making the directory $$1:
define make-dir
	echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'; \
	echo '~~~~~~~~~~~~~~~~~~~~~~~ creating directory ~~~~~~~~~~~~~~~~~~~~~~~'; \
	echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'; \
	${MKDIR_P} $1
endef

# synchronization routine for sources from src to build:
# with the option checksum enables, rsync does not synchronize based on a touch command!
define sync-sources
	echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'; \
	echo '~~~~~~~~~~~~~ syncronizing sources to build directory ~~~~~~~~~~~~'; \
	echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'; \
	${RSYNC} --verbose --checksum --recursive --human-readable --progress $(SRC_DIR)/ $(BUILD_DIR)
endef

# compiles $1 from m4 to tikz (through pic)
define run-m4
	echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'; \
	echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~ calling m4 ~~~~~~~~~~~~~~~~~~~~~~~~~~~'; \
	echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'; \
	${M4} $1 | ${DPIC_G} > $2
endef

# subtitutes \begin{tikzpicture} ... \end{tikzpicture} with \begin{scope} ... \end{scope} in $1
define substitute-tikzpicture-scope
	echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'; \
	echo '~~~~ replacing tikzpicture environment with scope environment ~~~~'; \
	echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'; \
	${SED} '/begin{tikzpicture}/s/tikzpicture/scope/' $1 > $1.tmp && mv $1.tmp $1; \
	${SED} '/end{tikzpicture}/s/tikzpicture/scope/' $1 > $1.tmp && mv $1.tmp $1
endef

# add to a dependency file where $$1 is dependent on <same basename as $$1>.$$2
# <basename> is the whole path without file extension.
# Function $(basename ) is applied twize to deal with .tikz.tex extensions
# if the dependency file does not exist, it is created
define write-dependency-in-dep-file
	echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'; \
	echo '~~~~~~~~~~~~~ checking dependencies and adding them  ~~~~~~~~~~~~~'; \
	echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'; \
	$(if $(wildcard $1),\
		$(if $(findstring $(basename $(basename $1))$(TIKZ_FILEXT): $2,$(shell ${CAT} $1)),\
			,\
			$(shell echo '$(basename $(basename $1))$(TIKZ_FILEXT): $2' >> $1)\
		),\
		$(shell echo '$(basename $(basename $1))$(TIKZ_FILEXT): $2' > $1)\
	)
endef

# the SED statement (or both echo & SED) potentially needs a foreach loop:
# and the echo statements needs an if $1 exists, don't do the echo loop
# (to provide overwriting the handwritten .tikz.tex files in the tikz directory)

#		$(if $(findstring \\tikzsetnextfilename{$(basename $(basename $(notdir $1))),$(shell cat $1)),,)\


define write-header-in-tikz-file
	$(if $(findstring \tikzsetexternalprefix,$(if $(wildcard $1),$(shell cat $1),)),\
		echo '~~~~~ file $1 ~~~~~';\
		echo '~~~~~ already contains a ~~~~~';\
		echo '~~~~~ \\tikzsetexternalprefix command ~~~~~~';\
		echo '~~~~~ will not write a new one ~~~~~';\
		echo '~~~~~ (even the existing one is in a comment) ~~~~~~';\
	,\
		(echo '\\tikzsetexternalprefix{$(TIKZ_PREFIX)}' && ${CAT} $1) > $1.tmp && mv $1.tmp $1;\
	)\
	$(if $(findstring \tikzset{external/system call,$(if $(wildcard $1),$(shell cat $1),)),\
		echo '~~~~~ file $1 ~~~~~';\
		echo '~~~~~ already contains a ~~~~~';\
		echo '~~~~~ \\tikzset{external/system call} command ~~~~~~';\
		echo '~~~~~ will not write a new one ~~~~~';\
		echo '~~~~~ (even the existing one is in a comment) ~~~~~~'\
	,\
		(echo '\\tikzset{external/system call={$(TIKZ_CMD)}}' && ${CAT} $1) > $1.tmp && mv $1.tmp $1\
	)
endef

#	$(if $(findstring \tikzsetnextfilename,$(shell cat $1)),\
#		echo '~~~~~ file $1 ~~~~~';\
#		echo '~~~~~ already contains a ~~~~~';\
#		echo '~~~~~ \\tikzsetexternalprefix command ~~~~~~';\
#		echo '~~~~~ will not write a new one ~~~~~';\
#		echo '~~~~~ (even the existing one is in a comment) ~~~~~~';\
#	,\
#		(echo '\\tikzsetnextfilename{$(TIKZ_SETNEXTFILENAME)}' && ${CAT} $1) > tmp && mv tmp $1;\
#	)\


#\def\tikzexternalrealjob{presentation}\input{presentation}
#echo '~~~~~~ file $1 already contains an \\tikzexternalprefix command ~~~~~~';\

# reference ??
#	${SED} '1s/^/\\\tikzsetexternalprefix\{$(TIKZ_DIR)\/$(basename $(basename $(notdir $1)))\_\}/' $1 > tmp && mv tmp $1;\
#	${SED} '1s/^/\\\tikzset\{external\/system call=\{$(TIKZ_CMD)\}\}/' $1 > tmp && mv tmp $1

# under debugging:
#	(echo '\\tikzsetexternalprefix{$(TIKZ_DIR)/$(basename $(basename $(notdir $1)))_}' && ${CAT} $1) > tmp && mv tmp $1;\
#	(echo '\\tikzset{external/system call={$(TIKZ_CMD)}}' && ${CAT} $1) > tmp && mv tmp $1
# works:
#	${SED} '1s/^/\\\tikzsetexternalprefix\{$(TIKZ_DIR)\/$(basename $(basename $(notdir $1)))\_\}/' $1 > tmp && mv tmp $1;\
#	${SED} '1s/^/\\\tikzset\{external\/system call=\{$(TIKZ_CMD)\}\}/' $1 > tmp && mv tmp $1
#
#	${SED} '1s/^/\\\tikzsetnextfilename\{$(basename $(basename $(notdir $1)))\}/' $1 > tmp && mv tmp $1;\
#	${SED} '1s/^/\\\tikzsetexternalprefix\{$(TIKZ_DIR)\/$(basename $(basename $(notdir $1)))\_\}/' $1 > tmp && mv tmp $1;\

# creates a file $$1, which contains a tikzpicture environment around $$2
define write-tikz-file-for-circuit-macros
	$(if $(wildcard $1),,\
		echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'; \
		echo '~~ writing .tikz.tex file to include output from circuit macros ~~'; \
		echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'; \
		echo '\\begin{tikzpicture}\n\t\input{tikz/m4/$2}\n\end{tikzpicture}' > $1; \
		$(call write-header-in-tikz-file,$1)\
	)
endef


#		${SED} '1s/^/\\\tikzsetnextfilename\{$(basename $(basename $(notdir $1)))\}/' $1 > tmp && mv tmp $1;\
#		${SED} '1s/^/\\\tikzset\{external\/system call=\{$(TIKZ_CMD)\}\}/' $1 > tmp && mv tmp $1\

#	echo '\\begin{tikzpicture}\n\t\input{$2}\n\end{tikzpicture}' > $1; \
# \\\tikzsetnextfilename{$(basename $(basename $1))}\n

# the SED statement needs a foreach loop:
# hmmm, I guess the whole thing needs a foreach loop?
# Deleting the findstring might be really good:
# allows overwriting the filename in the tikz.tex file manually
# but does not prevent insertion, if the string is in a comment!
# Right now it's no good!
# add --use-make to the compiler call!

####
# use project.makefile + write a preamble to each .tikz file using
# \tikzset
# {
#   external/system call = {},
#   external/shell escape = {}
#  }
# \tikzsetnextfilename{}
#### First try in buck.tikz.tex:
#%\tikzset%
#%{%
#%  external/system call =%
#%  {%
#%    latexmk \tikzexternalcheckshellescape
#%    -xelatex -g -use-make -file-line-error-halt-on-error -use-make -jobname "\image" %"\texsource"%
#%  }%
#%}%

# adds information at the top of file $$1
# with output filename and compiler information
define add-tikz-header
	echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'; \
	echo '~~ adding header to tikzfile (e.g. filename and compiler call) ~~~'; \
	echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'; \
	$(if $(wildcard $1),\
		$(foreach tikzfile,$1,\
			echo '~~ Adding header to: $(tikzfile)';\
			$(call write-header-in-tikz-file,$(tikzfile));\
		)\
		echo '~~~~~ This was the last tikzfile ~~~~~~'\
	,\
	$(shell echo 'file $1 does not exist') \
	)
endef

# th if $(wildacrd $1) still does not make sense:
# its a string of files in there,
# and if there would be none,
# I guess we wouldn't be here!

#		$(if $(findstring \\tikzsetnextfilename{$(basename $(basename $(notdir $1))),$(shell cat $1)),,)\
#				$(MAKE) $(MAKEFLAGS) $(BUILD_M4_DIR)/$(notdir $(tikzfile));\

#				${SED} '1s/^/\\\tikzsetnextfilename\{$(basename $(basename $(notdir $(tikzfile))))\}/' $(tikzfile) > tmp && mv tmp $(tikzfile);\
#				${SED} '1s/^/\\\tikzset\{external\/system call=\{$(TIKZ_CMD)\}\}/' $(tikzfile) > tmp && mv tmp $(tikzfile);\

# ${SED} '1s/^/\\\tikzsetnextfilename\{$(basename $(basename $(notdir $(tikzfile))))\}/' $(tikzfile) > tmp && mv tmp $(tikzfile))\


# the $(if $(wildcard $1) does not make sense: we would not be here if it was not existing, the way it's called now!

#good information about sed:
# https://unix.stackexchange.com/questions/99350/how-to-insert-text-before-the-first-line-of-a-file
# hitting return, insert only with extra argument on Mac OS,
# also solution with echo && cat there!
# working without \ and variables:
# (echo '\tbd{test from local}' && cat $1) > tmp && mv tmp $1\
# working without \ and variables:
# ${SED} '1s/^/$2/' $1 > tmp && mv tmp $1\
# 
#${SED} '1s/^/$2/' $1 > tmp && mv tmp $1\
#$(shell echo "'$2'; $(shell cat $1)") > $1
#1iText to add\
# ${SED} '1s/^/$2/' $1 > tmp && mv tmp $1\
#echo "$(shell echo -n '$2'); $(shell cat $1)" > $1 \

# idea: run latex with option -cd with absolut path:
# using $(CURDIR) maybe together with $(PWD)?
# use $(nodir $(basename $1)) instead of $(basename $1),
# options with directories (dep & output) set to current directory
# check the dependency file for dependencies to non build directories?
# this is going to make it worse!!!

# runs latexmk with the defined options on argument $$1
# further arguemnts for latexmk may be based through a second optional argument $$2
define run-latexmk
	echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'; \
	echo '~~~~~~~~~~~~~~~~~~~~~~~~~ calling latexmk ~~~~~~~~~~~~~~~~~~~~~~~~'; \
	echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'; \
	${LATEXMK} $(LATEXMK_OPT) $(if $2,$2,) $(basename $1);\
	$(call remove-dev-null-dependency,$(basename $1).dep);\
	$(call correct_makefile,$(basename $1).makefile);\
	$(call add-dep-on-tikzset-file-in-makefile,$(basename $1).makefile);\
	$(call add-m4-target-in-tikz-makefile,$(basename $1).makefile)
endef

# removes /dev/null from depedency file
define remove-dev-null-dependency
	if [ -e '$1' ]; then \
		echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'; \
		echo '~~~~~~~~~~~ removing /dev/null from dependency file ~~~~~~~~~~~~~~'; \
		echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'; \
		${GREP} -v "/dev/null" $1 > $1.tmp && mv $1.tmp $1; \
	fi
endef

# correction in makefile (from Tikz externalization) after LaTeX run
# replaces ^^I with a tab in makefile - necessary for xetex runs
# replaces .epsi with .ps in makefile - necessary for latex runs
ifeq ($(LATEX),xelatex)
define correct_makefile
	if [ -e '$1' ]; then \
		echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'; \
		echo '~~~~~~~~~~~~~ corrections in makefile after XeTeX ~~~~~~~~~~~~~~~~'; \
		echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'; \
		${SED} 's/^\^\^I/	/' $1 > $1.tmp && mv $1.tmp $1; \
	fi
endef
else ifeq ($(LATEX),latex)
define correct_makefile
	if [ -e '$1' ]; then \
		echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'; \
		echo '~~~~~~~~~~~~~ corrections in makefile after LaTeX ~~~~~~~~~~~~~~~~'; \
		echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'; \
		${SED} 's/.epsi/.ps/' $1 > $1.tmp && mv $1.tmp $1; \
	fi
endef
else
define correct_makefile
	if [ -e '$1' ]; then \
		echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'; \
		echo '~~~~~~~~~~~~~~~~ makefile after LaTeX is good ~~~~~~~~~~~~~~~~~~~~'; \
		echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'; \
	fi
endef
endif

# the global file for tikz settings is added as a dependency on all tikz->pdf targets in tikz makefile
define add-dep-on-tikzset-file-in-makefile
	if [ -e '$1' ]; then \
		echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'; \
		echo '~~~~ adding dependency on global tikzset-file in tikz-makefile ~~~'; \
		echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'; \
		(${CAT} $1 && echo '$$(wildcard tikz/*.pdf): $(TIKZSET_FILE)') > $1.tmp && mv $1.tmp $1; \
	fi
endef

# adding a target in tikz-makefile, who's receipe is pointing back towards the m4 targets in this Makefile
define add-m4-target-in-tikz-makefile
	if [ -e '$1' ]; then\
		echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~';\
		echo '~~~ adding m4 target in tikz-makefile: calling main Makefile ~~~~~';\
		echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~';\
		(${CAT} $1 && echo 'tikz/m4/%$(TIKZ_FILEXT):\n\t$(MAKE) -C $(CURDIR) -f $(THIS_MAKEFILE_NAME) $(BUILD_DIR)/$$@') > $1.tmp && mv $1.tmp $1;\
	fi
endef

define remove-def-dpthimport-from-aux
	if [ -e '$1' ]; then\
		echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~';\
		echo '~~~~~~~ removing def and dpthimport (twice) from aux file ~~~~~~~~';\
		echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~';\
		${SED} '/citation/s/ //g' $1 > $1.tmp && mv $1.tmp $1;\
		${SED} 's/\\def\\dpthimport//' $1 > $1.tmp && mv $1.tmp $1;\
		${SED} 's/\\dpthimport//g' $1 > $1.tmp && mv $1.tmp $1;\
	fi
endef

#		${CP} $(BUILD_TIKZ_DIR)/*.dpth $(BUILD_TIKZ_DIR)/*.olddpth;\
#		${RM} $(BUILD_TIKZ_DIR)/*.dpth;\
#		for dpthfile in find -f $(BUILD_TIKZ_DIR) -name "*.dpth"; do mv -f "$$dpthfile" "$$dpthfile-old" ;done;\
#		echo '$$(wildcard $(BUILD_TIKZ_DIR)/*.dpth) is $(wildcard $(BUILD_TIKZ_DIR)/*.dpth)';\
#		$(foreach dpthfile,$(wildcard $(BUILD_TIKZ_DIR)/*.dpth),\
#			echo '$$(dpthfile) is $(dpthfile)';\
#			mv -f $(dpthfile) $(dpthfile)old;\
#		)\
#mv -- "$f" "${f%.html}.php"
# this one works for replacing the spaces:
#		${SED} '/citation/s/ //g' $1 > tmp && mv tmp $1;\
#		${SED} 's/\\def \\dpthimport \{\\citation \{.*\}\}\\dpthimport/\\citation\{.*\}/g' $1 > tmp && mv tmp $1;\
#		${SED} 's/\\def \\dpthimport \\citation \{.*\}\\dpthimport/\\citation\{.*\}/g' $1 > tmp && mv tmp $1;\
#		${SED} 's/\\def \\dpthimport//' $1 > tmp && mv tmp $1;\
#		${SED} 's/\\dpthimport//' $1 > tmp && mv tmp $1;\

#		echo '$$(MAKEFLAGS) is $(MAKEFLAGS)';\
# removing the makefile flags - something wrong with option -j and simplifying the path. Still works!
# For latexmk to find the makefile, it needs to have a standard name. here makefile (parent is Makefile)
#tikz/m4/%.tikz.tex:
#	make -C /Users/aknott/Documents/undervisning/latex_slides_template -f Makefile ./build/$@

# this worked by manually entering it into makefile
# tikz/m4/%.tikz.tex:
#	cd ..;\
#	/Applications/Xcode.app/Contents/Developer/usr/bin/make  --jobserver-fds=3,6 - --jobserver-fds=3,6 -j 8 -f /Users/aknott/Documents/undervisning/latex_slides_template/Makefile build/$@

# potentially add a check, if makefile exists?
# why would I - some error routine is catching this one anyways already

# Processing the tikz makefile
define process-tikz-makefile
	if [ -e '$1' ]; then\
		echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~';\
		echo '~~~~~~~~~~~~ processing makefile for TikZ pictures ~~~~~~~~~~~~~~~';\
		echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~';\
		$(CP) $1 $(dir $1)/makefile;\
		$(call run-makefile-for-tikz,$1);\
	fi
endef

# executing the .makefile ($$1), that was generated to compile the tikz figures
define run-makefile-for-tikz
	${CD} $(BUILD_DIR) && \
	echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~' && \
	echo '~~~~~~~~~~~~~~~ building pdfs from externalized tikz ~~~~~~~~~~~~~' && \
	echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~' && \
	$(MAKE) $(MAKEFLAGS) -f $(notdir $1) && \
	${CD} $(CURDIR)
endef

# copies the file $$1 into the output directory
define copy-output
	echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'; \
	echo '~~~~~~~~~~~~~~~~~~~~~~~~~ copying output ~~~~~~~~~~~~~~~~~~~~~~~~~'; \
	echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'; \
	${CP} $1 $(OUT_DIR)/
endef

# summarizes all errors from latex log files, if they exist:
define summarize-errors-from-logs
	if [ -e '$1' ]; then\
		echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~';\
		echo '~~~~~~~~ collection of all errors in LaTeX log files ~~~~~~~~~~~~~';\
		echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~';\
	fi
endef

#use SED for this. Exit status of grep of 1, if nothing is found crashes make!
#		${GREP} -e ".*:.[1-9999]:.*" -A 1 -B 1 $1;\
#		${GREP} -e "error" -A 1 -B 1 $1;\


# remove everything generated during compilation including the resulting pdf's:
define clean
	echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'; \
	echo '~~~~~~~~~ deleting everything in the build directory ~~~~~~~~~~~~~'; \
	echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'; \
	${RM} -rf $(BUILD_DIR)/*
endef

# remove all the files generated through compliation, but not the resulting pdf's:
define cleanall
	echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'; \
	echo '~~~~~~~~~ deleting everything, that got generated ~~~~~~~~~~~~~~~~'; \
	echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'; \
	${RM} -rf $(OUT_DIR) $(BUILD_DIR)
endef

# display the help for this Makefile
define display-help
	echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'; \
	echo '~~~~~~~~~~~~~~~~~~~ displaying help of Makefile ~~~~~~~~~~~~~~~~~~'; \
	echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'; \
	echo $(HELP)
endef

#### WHERE TO FIND THE SOURCES ####

#vpath %.tex ./src:./src/chapters:./src/include
#vpath %$(TIKZ_FILEXT) ./src/*/tikz:./src/tikz
#vpath %.m4 $(wildcard .src/*/m4):./src/m4
vpath %.tex $(SRC_DIR):$(SRC_DIR)/chapters:$(SRC_DIR)/include
vpath %$(TIKZ_FILEXT) $(wildcard $(SRC_DIR)/*/tikz):$(SRC_DIR)/tikz
vpath %.m4 $(wildcard $(SRC_DIR)/*/m4):$(SRC_DIR)/tikz/m4:$(SRC_DIR)/m4:$(SRC_DIR)/include/m4:$(SRC_DIR)/include/tikz/m4

###############
#### RULES ####
###############

#### PHONY RULES ####
# all local .PHONY:
.PHONY: all presentation script alltikz directories tikz_directory clean cleanall help
# PHONY to avoid make to search for build rules of files, that do not need to get built:
.PHONY: $(wildcard *.nav) $(wildcard *.toc) $(wildcard *.loe)

# delete all suffix rules to disable all the built in c cpp and other rules
.SUFFIXES:

# make all targets secondary (and therefore intermediate) to prevent automatic deletion of any targets
.SECONDARY:

# The first rule in a Makefile is the one executed by default ("make"). It
# should always be the "all" rule, so that "make" and "make all" are identical.
all: presentation script

# calling the rule from another shell makes sure, that the dependency file
# is included through the include command in this Makefile AFTER the sources
# are sync'ed to the build directory
presentation: $(BUILD_DIR)/last_sync.txt
	$(MAKE) $(MAKEFLAGS) $(BUILD_DIR)/$(PRESENTATION_FILE).pdf

# see presentation:
script: $(BUILD_DIR)/last_sync.txt
	$(MAKE) $(MAKEFLAGS) $(BUILD_DIR)/$(SCRIPT_FILE).pdf

# builds all tikz pictures by executing the makefile in the build directory
alltikz: $(wildcard $(BUILD_TIKZ_DIR)/*$(TIKZ_FILEXT)) | tikz_directory
	@echo '$$(wildcard $(BUILD_TIKZ_DIR)/*$(TIKZ_FILEXT)) is $(wildcard $(BUILD_TIKZ_DIR)/*$(TIKZ_FILEXT))';\
	$(call run-makefile-for-tikz,$(wildcard $(BUILD_DIR)/*.makefile))

renamedpth: | tikz_directory
	@echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~';\
	echo '~~~~~~~~~~~~~~~~ renaming dpth files into dpth_old ~~~~~~~~~~~~~~~';\
	echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~';\
	for dpthfile in $(shell ls $(BUILD_TIKZ_DIR)/*.dpth); do mv -f $${dpthfile} $${dpthfile}_old; done

#cp -f $(BUILD_TIKZ_DIR)/*.dpth $(BUILD_TIKZ_DIR)/*.old_dpth
#rm -f $(BUILD_TIKZ_DIR)/*.dpth

# implement continuous compilation for any pdf - WYSIWYG-feature
# use OPT_CONT

# creates the M4 directory inside the tikz directory
m4_directory: $(BUILD_M4_DIR) | $(BUILD_TIKZ_DIR)

# creates the tikz directory inside the build directory
tikz_directory: $(BUILD_TIKZ_DIR) | $(BUILD_DIR)

# creates the build and output directory
directories: $(BUILD_DIR) $(OUT_DIR)

#### BUILD RULES ####

$(BUILD_M4_DIR):
	@$(call make-dir,$@)

$(BUILD_TIKZ_DIR):
	@$(call make-dir,$@)

$(BUILD_DIR):
	@$(call make-dir,$@)

$(OUT_DIR):
	@$(call make-dir,$@)

# synchronizing sources and writing the last-sync.txt file
$(BUILD_DIR)/last_sync.txt: $(SOURCES) | directories
	@$(call sync-sources)
	@date > $@

# building tikz files from m4 files
-include $(M4DEP_FILES)
#.SECONDARY: $(wildcard $(BUILD_M4_DIR)/*$(TIKZ_FILEXT))
$(BUILD_M4_DIR)/%$(TIKZ_FILEXT): %.m4 $(BUILD_TIKZ_DIR)/%.dep $(M4INCLUDE_FILES) | m4_directory
	$(if $(wildcard $<), \
		@$(call run-m4,$<,$@) && \
		$(call substitute-tikzpicture-scope,$@), \
	)

# adding a project library file for m4 defines

# building the dpendency files for m4 dircuit macros
#.SECONDARY: $(wildcard $(BUILD_TIKZ_DIR)/*.dep)
$(BUILD_TIKZ_DIR)/%.dep: %.m4 | tikz_directory
	@$(call write-dependency-in-dep-file,$@,$<)

# the @$(if $(wildcard $@) call should be redundant in the next rule:
# if the file exists, the receipies of this target is not executed!
# the add-tikz-header statement should be dealt with through the pdf-generation receipe!


# potential challenge here with the TIKZSET_FILE as dependency here: if it doesn't exist, the script wants to create it,
# however there is no target for it. And it isn't needed here any more, as this is taking care of in the tikz-makefile:

# building the outer tikz.tex files for the m4 circuit macros generated .tikz.tex files, and adding the header
-include $(TIKZDEP_FILES)
#.SECONDARY: $(wildcard $(BUILD_TIKZ_DIR)/*$(TIKZ_FILEXT))
$(BUILD_TIKZ_DIR)/%$(TIKZ_FILEXT): $(BUILD_M4_DIR)/%$(TIKZ_FILEXT) $(TIKZSET_FILE) | tikz_directory
	@$(call write-tikz-file-for-circuit-macros,$@,$(<F))

# needs a check if makefile exists:

# build all tikz pictures:
# implemented straight above, so unnecesary here:
#$(BUILD_TIKZ_DIR)/%.pdf:

#$(wildcard $(BUILD_TIKZ_DIR)/*$(TIKZ_FILEXT))

# building of pdf files:
-include $(TEXDEP_FILES)
$(BUILD_DIR)/%.pdf: %.tex | directories
	@$(call add-tikz-header,$(wildcard $(BUILD_TIKZ_DIR)/*$(TIKZ_FILEXT)));\
	$(call run-latexmk,$@);\
	$(call process-tikz-makefile,$(basename $@).makefile);\
	$(call run-latexmk,$@);\
	$(call copy-output,$@);\
	$(call summarize-errors-from-logs,$(basename $@).log)

#	$(call summarize-errors-from-logs,$(wildcard $(BUILD_TIKZ_DIR)/*.log));\

#	$(CP) $(basename $@).makefile $(BUILD_DIR)/makefile;\
#	$(call run-makefile-for-tikz,$*.makefile);\


#;\
#	${GREP} -e 'Error' $(wildcard $(BUILD_TIKZ_DIR)*.log $(BUILD_DIR).*log)

#	$(shell echo '\\pgfexternal\@restore' && ${CAT} $(basename $@).aux > tmp && mv tmp $(basename $@).aux);\
#	$(call remove-def-dpthimport-from-aux,$(basename $@).aux);\
#	$(MAKE) $(MAKEFLAGS) renamedpth;\
#	$(call run-latexmk,$@,-usepretex="\\nofiles");\

#	ls $(BUILD_TIKZ_DIR)/*.dpth;\
#	for dpthfile in $(shell ls $(BUILD_TIKZ_DIR)/*.dpth); do echo $${dpthfile}; done;\
#	$(foreach dpthfile,$(shell find $(BUILD_TIKZ_DIR) -name '*.dpth'),\
#		echo '$$(dpthfile) is $(dpthfile)';\
#	);\
#	$(foreach dpthfile,$(shellfind $(BUILD_TIKZ_DIR) -name '*.dpth'),\
#		echo '$$(dpthfile) is $(dpthfile)';\
#		echo 'This was the last depthfile!'\
#	)\
#	$(foreach dpthfile,$(shell find $(BUILD_TIKZ_DIR) -name "*.dpth"),\
#		echo '$$(dpthfile) is $(dpthfile)';\
#	);\
#	echo '$$(shell find $(BUILD_TIKZ_DIR) -name "*.dpth") is $(shell find $(BUILD_TIKZ_DIR) -name "*.dpth")';\
#		${CP} $(BUILD_TIKZ_DIR)/*.dpth $(BUILD_TIKZ_DIR)/*.olddpth;\
#		${RM} $(BUILD_TIKZ_DIR)/*.dpth;\
#		for dpthfile in find -f $(BUILD_TIKZ_DIR) -name "*.dpth"; do mv -f "$$dpthfile" "$$dpthfile-old" ;done;\
#		echo '$$(wildcard $(BUILD_TIKZ_DIR)/*.dpth) is $(wildcard $(BUILD_TIKZ_DIR)/*.dpth)';\
#		$(foreach dpthfile,$(wildcard $(BUILD_TIKZ_DIR)/*.dpth),\
#			echo '$$(dpthfile) is $(dpthfile)';\
#			mv -f $(dpthfile) $(dpthfile)old;\
#		)\
#mv -- "$f" "${f%.html}.php"

#		echo '$$(wildcard $(BUILD_TIKZ_DIR)/*.dpth) is $(wildcard $(BUILD_TIKZ_DIR)/*.dpth)';\
#		$(foreach dpthfile,$(wildcard $(BUILD_TIKZ_DIR)/*.dpth),\
#			echo '$$(dpthfile) is $(dpthfile)';\
#			mv -f $(dpthfile) $(dpthfile)old;\
#		)\

#; \
#	$(MAKE) $(MAKEFLAGS) alltikz;\

#; \
#	$(call run-latexmk,$@); \
#	$(call copy-output,$@)

#$(call run-makefile-for-tikz,$*.makefile) && \

#### remember: %.tikz can be built from %.m4 or sources
####
# hmm, well actually what we are building is pdf-files - just in the subdirectory tikz/
# Furthermore it builds:
# .dep (dependency file)
# .depth (references, labels and citations inside the picture) same as .dpth???
# .dpth (for labels in tikz picutres: includs everything, that needs to be copied to the aux file in the main document - needs 2 times recompilation of main document if changed -> first to transfer .dpth to main.aux, then for main.pdf to include the changes in main.aux. Changes in main.aux should generate the warning "Labels have changed. Recompile", which latexmk should catch.)
# .log (logfile)
# .md5 (the md5 checksum of the last build for comparing if a new build is necessary)
# .pdf (the picture file itself)
####
# might want to build intermediate .tex files from the original .tikz files, which add
# \tikzsetnextfilename{mygraph} to change the filename to something human readable
# this one is still missing for the m4 created files as well
####
# another feature would be really really good:
# having a file with all the tikzset commands (which are in the preamble now).
# (also need such one for the m4 environment for global definitions)!
# All tikz pictures shall depend on this one (can be declared in the variable section of this file)
# so if any of the project specific
####
# How do we make sure, that the right amount of recompilations is run with citations inside the picture (and linked to the main document)?


# modyfing the tikz files from the source to change file names of the created file
# for human readability and specify the compiler taking care of the necessary multiple
# compilations for included labels, references and citations in tikzfigures.
# Furthermore all m4 files are processed into tikz files upfront.
#
# at a later stage, when the dependency files are generated by tge externalizing library,
# the dependency files for each figure can be included here!


########### after change of .latexmkrc, the files
## "build/tikz/presentation-figure0.dpth" and "build/tikz/presentation-figure0.pdf"
## are missing in the presentation.dep file; maybe even more of those are necessary?!?
## might have to do with writing the presentation.figlist and the presentation.makefile
## into presentation.fls (with flag INPUT) before executing the make command?!?


# NOW: call something to generate the externalized tikz-figures, if necessary. The existence of .makefile and .figlist should tell about that. Note, if externaliztion is run in mode "convert with system call", we should be done here.
#	@echo 'THIS IS GETTING EXECUTED FOR GENERATING THE TIKZ-PICTURES'
# PUT all the relevant stuff for running externalized tikz pictures here:
# Put all the header actions and so on in the tikz rule.
# Execute the makefile from here - or from tikz rule?!?! I guess it's a minor detail.
# Get all the compile, grep and sed magic into one command.
# Whenever compliation happens, this HAS to run afterwards!
#	$(call run-latex,$*)
#	$(LATEXMK_CMD)

## work with something like this:
##	$(MAKE) $(MAKEFLAGS) $(BUILDTIKZ_DIR).makefile
## based on that:
##	make",  "-j", "3", "-f", "$base.makefile

#### CLEAN UP ALL THAT MESS ####
clean:
	@$(call clean)

cleanall:
	@$(call cleanall)

help:
	@$(call display-help)
