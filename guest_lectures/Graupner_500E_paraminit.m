%% Motor initialization for 31012 Graupner 500E (old motor)

% Open simulation

% Motor rated specifications
Pin = 21;           % [W]
rated_voltage = 12;  % [V]
rated_speed = 5800;  % [RPM]
noload_speed = 12000; % [RPM]
rated_current = 2;  % [A]
max_current = 10;  % [A]
noload_current = 0.4;% [A]
rated_torque = 0.02647;  % [N.m]
stall_torque = 0.09285; % [N.m]
max_efficiency = 0.67;

% Electromechanical conversion constants
omega_r = 2*pi/60*rated_speed;
omega_nl = 2*pi/60*noload_speed;
kT = stall_torque / max_current;
kB = kT;

% Motor electrical constants
Rm = (rated_voltage - kB*omega_r) / rated_current;            % [ohm]
Lm = 4.416e-3;          % [H]

% Motor mechanical constants
Jm = 3.175e-6;           % [N.m.s^2]
Bm = 6.551e-6;          % [N.s/m] 


%% Openloop

graupner = tf(kT,[(Jm*Lm) (Jm*Rm+Lm*Bm) (Bm*Rm+kT^2)])
h = bodeplot(graupner)
h.setoptions('FreqUnits', 'Hz', 'grid', 'on');